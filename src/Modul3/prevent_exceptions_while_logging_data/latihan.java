/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Modul3.prevent_exceptions_while_logging_data;

import java.util.logging.Level;

/**
 *
 * @author REGI
 */
public class latihan {

    public static void main(String[] args) {
        // Noncompliant code
//        try {
//            // ...
//        } catch (SecurityException se) {
//            System.err.println(se);
//            // Recover from exception
//        }

        // Compliant code
        try {
            // ...
        } catch (SecurityException se) {
            logger.log(Level.SEVERE, se);
            // Recover from exception
        }
    }
}

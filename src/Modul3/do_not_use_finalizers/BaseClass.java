/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Modul3.do_not_use_finalizers;

/**
 *
 * @author REGI
 */
public class BaseClass {

    protected void finalize() throws Throwable {
        System.out.println("Superclass finalize!");
        doLogic();
    }

    public void doLogic() throws Throwable {
        System.out.println("This is super-class!");
    }
}

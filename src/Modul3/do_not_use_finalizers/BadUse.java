/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Modul3.do_not_use_finalizers;

import Modul3.ensure_that_constructors_do_not_call_overridable_method.SubClass;

/**
 *
 * @author REGI
 */
public class BadUse {

    public static void main(String[] args) {
        try {
            SubClass bc = new SubClass();
            // Artificially simulate finalization (do not do this)
            System.runFinalizersOnExit(true);
        } catch (Throwable t) {
            // Handle error
        }
    }
}

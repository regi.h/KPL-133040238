/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Modul3.do_not_use_finalizers;

import java.util.Date;

/**
 *
 * @author REGI
 */
public class SubClass extends BaseClass {

    private Date d; // Mutable instance field

    protected SubClass() {
        d = new Date();
    }
    // Noncompliant code
//  protected void finalize() throws Throwable {
//    System.out.println("Subclass finalize!");
//    try {
//      //  Cleanup resources
//      d = null;
//    } finally {
//      super.finalize();  // Call BaseClass's finalizer
//    }
//  }

    // Compliant code 1
    protected void finalize() throws Throwable {
        try {
            //...
        } finally {
            super.finalize();
        }
    }

    public void doLogic() throws Throwable {
        // Any resource allocations made here will persist

        // Inconsistent object state
        System.out.println(
                "This is sub-class! The date object is: " + d);
        // 'd' is already null
    }
}

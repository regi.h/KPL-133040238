/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Modul3.do_not_use_finalizers;

/**
 *
 * @author REGI
 */
public class Foo {
    // The finalizeGuardian object finalizes the outer Foo object

    private final Object finalizerGuardian = new Object() {
        protected void finalize() throws Throwable {
            // Finalize outer Foo object
        }
    };
    //...
}

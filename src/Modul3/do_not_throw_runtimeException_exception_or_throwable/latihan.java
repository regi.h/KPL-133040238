/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Modul3.do_not_throw_runtimeException_exception_or_throwable;

import java.io.IOException;

/**
 *
 * @author REGI
 */
public class latihan {
    // Noncompliant code 1
//    boolean isCapitalized(String s) {
//        if (s == null) {
//            throw new RuntimeException("Null String");
//        }
//        if (s.equals("")) {
//            return true;
//        }
//        String first = s.substring(0, 1);
//        String rest = s.substring(1);
//        return (first.equals(first.toUpperCase()) &&
//        rest.equals(rest.toLowerCase()));
//    }

    // Compliant code
    boolean isCapitalized(String s) {
        if (s == null) {
            throw new NullPointerException();
        }
        if (s.equals("")) {
            return true;
        }
        String first = s.substring(0, 1);
        String rest = s.substring(1);
        return (first.equals(first.toUpperCase())
                && rest.equals(rest.toLowerCase()));
    }

    // Noncompliant code 2
//    private void doSomething() throws Exception {
//        //...
//    }
    // Compliant code 2
    private void doSomething() throws IOException {
        //...
    }
}

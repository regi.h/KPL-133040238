/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Modul3.never_declared_a_class_method_that_hides_a_method_declared_in_a_superclass_or_superinterface;

/**
 *
 * @author REGI
 */
public class GrantAccess {

    // Noncompliant code
//    public static void displayAccountStatus() {
//        System.out.println("Account details for admin: XX");
//    }
    // Compliant code
    public void displayAccountStatus() {
        System.out.print("Account details for admin: XX");
    }
}

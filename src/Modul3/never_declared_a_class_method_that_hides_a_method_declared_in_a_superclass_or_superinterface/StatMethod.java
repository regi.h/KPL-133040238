/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Modul3.never_declared_a_class_method_that_hides_a_method_declared_in_a_superclass_or_superinterface;

/**
 *
 * @author REGI
 */
public class StatMethod {
    // Noncompliant code
//    public static void choose(String username) {
//        GrantAccess admin = new GrantAccess();
//        GrantAccess user = new GrantUserAccess();
//        if (username.equals("admin")) {
//            admin.displayAccountStatus();
//        } else {
//            user.displayAccountStatus();
//        }
//    }
//    
//    public static void main(String[] args) {
//        choose("user");
//    }

    // Compliant code
    public static void choose(String username) {
        GrantAccess admin = new GrantAccess();
        GrantAccess user = new GrantUserAccess();
        if (username.equals("admin")) {
            admin.displayAccountStatus();
        } else {
            user.displayAccountStatus();
        }
    }

    public static void main(String[] args) {
        choose("user");
    }
}

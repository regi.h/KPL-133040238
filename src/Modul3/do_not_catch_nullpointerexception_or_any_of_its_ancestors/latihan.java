/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Modul3.do_not_catch_nullpointerexception_or_any_of_its_ancestors;

/**
 *
 * @author REGI
 */
public class latihan {

    boolean isCapitalized(String s) {
        if (s == null) {
            throw new NullPointerException();
        }
        if (s.equals("")) {
            return true;
        }
        String first = s.substring(0, 1);
        String rest = s.substring(1);
        return (first.equals(first.toUpperCase())
                && rest.equals(rest.toLowerCase()));
    }
    // Noncompliant code
//    boolean isName(String s) {
//        try {
//            String names[] = s.split(" ");
//            if (names.length != 2) {
//                return false;
//            }
//            return (isCapitalized(names[0]) && isCapitalized(names[1]));
//        } catch (NullPointerException e) {
//            return false;
//        }
//    }

    // Compliant code 1
//    boolean isName(String s) {
//        if (s == null) {
//            return false;
//        }
//        String names[] = s.split(" ");
//        if (names.length != 2) {
//            return false;
//        }
//        return (isCapitalized(names[0]) && isCapitalized(names[1]));
//    }
    // Compliant code 2
    boolean isName(String s) /* Throws NullPointerException */ {
        String names[] = s.split(" ");
        if (names.length != 2) {
            return false;
        }
        return (isCapitalized(names[0]) && isCapitalized(names[1]));
    }
}

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Modul3.restore_prior_object_state_on_method_failure;

/**
 *
 * @author REGI
 */
public class Dimensions {

    private int length;
    private int width;
    private int height;
    static public final int PADDING = 2;
    static public final int MAX_DIMENSION = 10;

    public Dimensions(int length, int width, int height) {
        this.length = length;
        this.width = width;
        this.height = height;
    }
//    protected int getVolumePackage(int weight) {
//        length += PADDING;
//        width += PADDING;
//        height += PADDING;
//        try {
//            if (length <= PADDING || width <= PADDING || height <= PADDING ||
//            length > MAX_DIMENSION + PADDING || width > MAX_DIMENSION +
//            PADDING ||
//            height > MAX_DIMENSION + PADDING || weight <= 0 || weight > 20) {
//                throw new IllegalArgumentException();
//            }
//            int volume = length * width * height;
//            length -= PADDING; width -= PADDING; height -= PADDING; // Revert
//            return volume;
//            // Noncompliant code
////        } catch (Throwable t) {
////            MyExceptionReporter mer = new MyExceptionReporter();
////            mer.report(t); // Sanitize
////            return -1; // Non-positive error code
////        }
//            // Compliant code Rollback
//        } catch (Throwable t) {
//            MyExceptionReporter mer = new MyExceptionReporter();
//            mer.report(t); // Sanitize
//            length -= PADDING; width -= PADDING; height -= PADDING; // Revert
//            return -1;
//        }
//    }

    // Compliant code Finally Clause
//    protected int getVolumePackage(int weight) {
//        length += PADDING;
//        width  += PADDING;
//        height += PADDING;
//        try {
//            if (length <= PADDING || width <= PADDING || height <= PADDING ||
//            length > MAX_DIMENSION + PADDING ||
//            width > MAX_DIMENSION + PADDING ||
//            height > MAX_DIMENSION + PADDING ||
//            weight <= 0 || weight > 20) {
//                throw new IllegalArgumentException();
//            } 
//            int volume = length * width * height;
//            return volume;
//        } catch (Throwable t) {
//            MyExceptionReporter mer = new MyExceptionReporter();
//            mer.report(t); // Sanitize
//            return -1; // Non-positive error code
//        } finally {
//            // Revert
//            length -= PADDING; width -= PADDING; height -= PADDING;
//        }
//    }
    // Compliant code InputValidation
    protected int getVolumePackage(int weight) {
        try {
            if (length <= 0 || width <= 0 || height <= 0
                    || length > MAX_DIMENSION || width > MAX_DIMENSION || height > MAX_DIMENSION
                    || weight <= 0 || weight > 20) {
                throw new IllegalArgumentException(); // Validate first
            }
        } catch (Throwable t) {
            MyExceptionReporter mer = new MyExceptionReporter();
            mer.report(t); // Sanitize
            return -1;
        }

        length += PADDING;
        width += PADDING;
        height += PADDING;

        int volume = length * width * height;
        length -= PADDING;
        width -= PADDING;
        height -= PADDING;
        return volume;
    }

    public static void main(String[] args) {
        Dimensions d = new Dimensions(8, 8, 8);
        System.out.println(d.getVolumePackage(21)); // Prints -1 (error)
        System.out.println(d.getVolumePackage(19));
        // Prints 1728 (12x12x12) instead of 1000 (10x10x10)
    }
    // Compliant code Unmodified Object
}

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Modul3.ensure_that_constructors_do_not_call_overridable_method;

/**
 *
 * @author REGI
 */
public class SuperClass {
    // Noncompliant code
//    public SuperClass () {
//        doLogic();
//    }
//    
//    public void doLogic() {
//        System.out.println("This is superclass!");
//    }

    // Compliant code
    public SuperClass() {
        doLogic();
    }

    public final void doLogic() {
        System.out.println("This is superclass!");
    }
}

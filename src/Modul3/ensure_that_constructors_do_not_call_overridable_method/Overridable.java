/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Modul3.ensure_that_constructors_do_not_call_overridable_method;

/**
 *
 * @author REGI
 */
public class Overridable {

    public static void main(String[] args) {
        SuperClass bc = new SuperClass();
        // Prints "This is superclass!"
        SubClass sc = new SubClass();
        // Prints "This is subclass! The color is :null"
    }
}

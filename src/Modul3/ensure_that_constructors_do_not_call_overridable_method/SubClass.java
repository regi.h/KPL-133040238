/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Modul3.ensure_that_constructors_do_not_call_overridable_method;

/**
 *
 * @author REGI
 */
public class SubClass extends SuperClass

    {
    
    private String color;

    public SubClass() {
        super();
        this.color = null;
        color = "Red";
    }

    
    public void doLogic() {
        System.out.println("This is subclass! The color is :" + color);
        // ...
    }
}

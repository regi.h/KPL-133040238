/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Modul3.do_not_allow_untrusted_code_to_terminate_the_jvm;

/**
 *
 * @author REGI
 */
class PasswordSecurityManager extends SecurityManager {

    private boolean isExitAllowedFlag;

    public PasswordSecurityManager() {
        super();
        isExitAllowedFlag = false;
    }

    public boolean isExitAllowed() {
        return isExitAllowedFlag;
    }

    @Override
    public void checkExit(int status) {
        if (!isExitAllowed()) {
            throw new SecurityException();
        }
        super.checkExit(status);
    }

    public void setExitAllowed(boolean f) {
        isExitAllowedFlag = f;
    }
}


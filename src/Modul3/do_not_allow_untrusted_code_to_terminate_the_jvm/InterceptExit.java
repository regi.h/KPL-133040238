/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Modul3.do_not_allow_untrusted_code_to_terminate_the_jvm;

/**
 *
 * @author REGI
 */
// Noncomplian code
//public class InterceptExit {
//    public static void main(String[] args) {
//        // ...
//        System.exit(1); // Abrupt exit
//        System.out.println("This never executes");
//    }
//}
// Compliant code
public class InterceptExit {

    public static void main(String[] args) {
        PasswordSecurityManager secManager =
                new PasswordSecurityManager();
        System.setSecurityManager(secManager);
        try {
            // ...
            System.exit(1); // Abrupt exit call
        } catch (Throwable x) {
            if (x instanceof SecurityException) {
                System.out.println("Intercepted System.exit()");
                // Log exception
            } else {
                // Forward to exception handler
            }
        }
        // ...
        secManager.setExitAllowed(true); // Permit exit
        // System.exit() will work subsequently
        // ...
    }
}

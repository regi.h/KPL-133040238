/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Modul3.never_use_assertion_to_validate_method_arguments;

/**
 *
 * @author REGI
 */
public class latihan {
    // Noncompliant code
//    public static int getAbsAdd(int x, int y) {
//        assert x != Integer.MIN_VALUE;
//        assert y != Integer.MIN_VALUE;
//        int absX = Math.abs(x);
//        int absY = Math.abs(y);
//        assert (absX <= Integer.MAX_VALUE - absY);
//        return absX + absY;
//    }

    // Compliant code
    public static int getAbsAdd(int x, int y) {
        if (x == Integer.MIN_VALUE || y == Integer.MIN_VALUE) {
            throw new IllegalArgumentException();
        }
        int absX = Math.abs(x);
        int absY = Math.abs(y);
        if (absX > Integer.MAX_VALUE - absY) {
            throw new IllegalArgumentException();
        }
        return absX + absY;
    }
}

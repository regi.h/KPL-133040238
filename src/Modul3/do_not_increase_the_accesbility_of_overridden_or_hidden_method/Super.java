/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Modul3.do_not_increase_the_accesbility_of_overridden_or_hidden_method;

/**
 *
 * @author REGI
 */
public class Super {
    // Noncompliant code
//    protected void doLogic() {
//        System.out.println("Super invoked");
//    }

    // Compliant code
    protected final void doLogic() { // Declare as final
        System.out.println("Super invoked");
        // Do sensitive operations
    }
}

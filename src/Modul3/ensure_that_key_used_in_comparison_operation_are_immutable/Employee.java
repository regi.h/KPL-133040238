/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Modul3.ensure_that_key_used_in_comparison_operation_are_immutable;

import java.util.Calendar;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 *
 * @author REGI
 */
public class Employee {
    // Noncompliant code
//    private String name;
//    private double salary;
//    
//    Employee(String empName, double empSalary) {
//        this.name = empName;
//        this.salary = empSalary;
//    }
//
//    public void setEmployeeName(String empName) {
//        this.name = empName;
//    }
//
//    public void setSalary(double empSalary) {
//        this.salary = empSalary;
//    }
//    
//    @Override
//    public boolean equals(Object o) {
//        if (!(o instanceof Employee)) {
//            return false;
//        }
//
//        Employee emp = (Employee)o;
//        return emp.name.equals(name);
//    }
//    
//    public int hashCode() {return 0;}
//    Map<Employee, Calendar> map = new ConcurrentHashMap<Employee, Calendar>();

    // Compliant code
    private String name;
    private double salary;
    private final long employeeID; // Unique for each Employee

    Employee(String name, double salary, long empID) {
        this.name = name;
        this.salary = salary;
        this.employeeID = empID;
    }
// ...

    @Override
    public boolean equals(Object o) {
        if (!(o instanceof Employee)) {
            return false;
        }

        Employee emp = (Employee) o;
        return emp.employeeID == employeeID;
    }
    Map<Employee, Calendar> map = new ConcurrentHashMap<Employee, Calendar>();
}

// ...

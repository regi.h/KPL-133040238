/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Modul3.ensure_that_key_used_in_comparison_operation_are_immutable;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.Hashtable;

/**
 *
 * @author REGI
 */
public class Hash_Ser {
    // Compliant code

    public static void main(String[] args)
            throws IOException, ClassNotFoundException {
        Hashtable<Integer, String> ht = new Hashtable<Integer, String>();
        ht.put(new Integer(1), "Value");
        System.out.println("Entry: " + ht.get(1)); // Retrieve using the key

        // Serialize the Hashtable object
        FileOutputStream fos = new FileOutputStream("hashdata.ser");
        ObjectOutputStream oos = new ObjectOutputStream(fos);
        oos.writeObject(ht);
        oos.close();

        // Deserialize the Hashtable object
        FileInputStream fis = new FileInputStream("hashdata.ser");
        ObjectInputStream ois = new ObjectInputStream(fis);
        Hashtable<Integer, String> ht_in =
                (Hashtable<Integer, String>) (ois.readObject());
        ois.close();

        if (ht_in.contains("Value")) // Check whether the object actually exists in the Hashtable
        {
            System.out.println("Value was found in deserialized object.");
        }

        if (ht_in.get(1) == null) // Not printed
        {
            System.out.println(
                    "Object was not found when retrieved using the key.");
        }
    }
}

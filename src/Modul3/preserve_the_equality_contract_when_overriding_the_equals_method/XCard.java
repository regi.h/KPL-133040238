/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Modul3.preserve_the_equality_contract_when_overriding_the_equals_method;

/**
 *
 * @author REGI
 */
public class XCard {
    // Noncompliant code 
//    private String type;
//    public XCard(int number, String type) {
//        super(number);
//        this.type = type;
//    }
//    
//    public boolean equals(Object o) {
//        if (!(o instanceof Card)) {
//            return false;
//        }
//        // Normal Card, do not compare type
//        if (!(o instanceof XCard)) {
//            return o.equals(this);
//        }
//        // It is an XCard, compare type as well
//        XCard xc = (XCard)o;
//        return super.equals(o) && xc.type == type;
//    }
//    
//    public int hashCode() {
//        return 0;
//    }
//    
//    public static void main(String[] args) {
//        XCard p1 = new XCard(1, "type1");
//        Card p2 = new Card(1);
//        XCard p3 = new XCard(1, "type2");
//        System.out.println(p1.equals(p2)); // Returns true
//        System.out.println(p2.equals(p3)); // Returns true
//        System.out.println(p1.equals(p3)); // Returns false
//        // violating transitivity
//    }

    // Compliant code 1 delegation
    private String type;
    private Card card; // Composition

    public XCard(int number, String type) {
        card = new Card(number);
        this.type = type;
    }

    public Card viewCard() {
        return card;
    }

    public boolean equals(Object o) {
        if (!(o instanceof XCard)) {
            return false;
        }
        XCard cp = (XCard) o;
        return cp.card.equals(card) && cp.type.equals(type);
    }

    public int hashCode() {
        return 0;
    }

    public static void main(String[] args) {
        XCard p1 = new XCard(1, "type1");
        Card p2 = new Card(1);
        XCard p3 = new XCard(1, "type2");
        XCard p4 = new XCard(1, "type1");
        System.out.println(p1.equals(p2)); // Prints false
        System.out.println(p2.equals(p3)); // Prints false
        System.out.println(p1.equals(p3)); // Prints false
        System.out.println(p1.equals(p4)); // Prints true
    }
}

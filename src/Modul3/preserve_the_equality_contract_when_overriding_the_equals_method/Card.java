/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Modul3.preserve_the_equality_contract_when_overriding_the_equals_method;

/**
 *
 * @author REGI
 */
public class Card {
    // Noncompliant code
//    private final int number;
//    
//    public Card(int number) {
//        this.number = number;
//    }
//    
//    public boolean equals(Object o) {
//        if (!(o instanceof Card)) {
//            return false;
//        }
//        Card c = (Card)o;
//        return c.number == number;
//    }
//    
//    public int hashCode() {
//        return 0;
//    }

    // Compliant code class comparison
    private final int number;

    public Card(int number) {
        this.number = number;
    }

    public boolean equals(Object o) {
        if (!(o.getClass() == this.getClass())) {
            return false;
        }
        Card c = (Card) o;
        return c.number == number;
    }

    public int hashCode() {
        return 0;
    }
}

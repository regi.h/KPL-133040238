/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Modul3.preserve_the_equality_contract_when_overriding_the_equals_method;

/**
 *
 * @author REGI
 */
public class CaseInsensitiveString {
    // Bagian 1
    // Noncompliant code 1
//    private String s;
//    
//    public CaseInsensitiveString(String s) {
//        if (s == null) {
//            throw new NullPointerException();
//        }
//        this.s = s;
//    }
//    // This method violates symmetry
//    
//    public boolean equals(Object o) {
//        if (o instanceof CaseInsensitiveString) {
//            return s.equalsIgnoreCase(((CaseInsensitiveString)o).s);
//        }
//        if (o instanceof String) {
//            return s.equalsIgnoreCase((String)o);
//        }
//        return false;
//    }
//    // Comply with MET09-J
//    public int hashCode() {
//        return 0;
//    }
//    
//    public static void main(String[] args) {
//        CaseInsensitiveString cis = new CaseInsensitiveString("Java");
//        String s = "java";
//        System.out.println(cis.equals(s)); // Returns true
//        System.out.println(s.equals(cis)); // Returns false
//    }

    // Compliant code 1
    private String s;

    public CaseInsensitiveString(String s) {
        if (s == null) {
            throw new NullPointerException();
        }
        this.s = s;
    }

    public boolean equals(Object o) {
        return o instanceof CaseInsensitiveString
                && ((CaseInsensitiveString) o).s.equalsIgnoreCase(s);
    }

    public int hashCode() {
        return 0;
    }

    public static void main(String[] args) {
        CaseInsensitiveString cis = new CaseInsensitiveString("Java");
        String s = "java";
        System.out.println(cis.equals(s)); // Returns false now
        System.out.println(s.equals(cis)); // Returns false now
    }
}

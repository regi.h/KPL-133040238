/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Modul3.preserve_the_equality_contract_when_overriding_the_equals_method;

import java.awt.RenderingHints;
import java.security.interfaces.RSAKey;
import java.security.interfaces.RSAPrivateKey;
import java.util.Arrays;

/**
 *
 * @author REGI
 */
public class latihan {
    // Noncompliant code
//    private static boolean keysEqual(Key key1, Key key2) {
//        if (key1.equals(key2)) {
//            return true;
//        }
//        return false;
//    }

    // Compliant code
    private static boolean keysEqual(RenderingHints.Key key1, RenderingHints.Key key2) {
        if (key1.equals(key2)) {
            return true;
        }
        if (Arrays.equals(key1.getEncoded(), key2.getEncoded())) {
            return true;
        }
        // More code for different types of keys here.
        // For example, the following code can check if
        // an RSAPrivateKey and an RSAPrivateCrtKey are equal:
        if ((key1 instanceof RSAPrivateKey) && (key2 instanceof RSAPrivateKey)) {
            if ((((RSAKey) key1).getModulus().equals(((RSAKey) key2).getModulus())) && (((RSAPrivateKey) key1).getPrivateExponent().equals(((RSAPrivateKey) key2).getPrivateExponent()))) {
                return true;
            }
        }
        return false;
    }
}

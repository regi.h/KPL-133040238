/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Modul3.do_not_complete_abruptly_from_a_finally_block;

import java.io.IOException;

/**
 *
 * @author REGI
 */
// Noncompliant code
//public class TryFinally {
//    private static boolean doLogic() {
//        try {
//            throw new IllegalStateException();
//        } finally {
//            System.out.println("logic done");
//            return true;
//        }
//    }
//}
// Compliant code 1
//class TryFinally {
//    private static boolean doLogic() {
//        try {
//            throw new IllegalStateException();
//        } finally {
//            System.out.println("logic done");
//        }
//        // Any return statements must go here;
//        // applicable only when exception is thrown conditionally
//    }
//}
// Compliant code 2
class TryFinally {

    private static boolean doLogic() {
        try {
            throw new IllegalStateException();
        } finally {
            int c;
            try {
                while ((c = input.read()) != -1) {
                    if (c > 128) {
                        break;
                    }
                }
            } catch (IOException x) {
                // Forward to handler
            }
            System.out.println("logic done");
        }
        // Any return statements must go here; applicable only when exception is thrown conditionally
    }
}
}

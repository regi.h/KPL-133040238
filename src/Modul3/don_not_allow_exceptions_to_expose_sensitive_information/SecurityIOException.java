/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Modul3.don_not_allow_exceptions_to_expose_sensitive_information;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

/**
 *
 * @author REGI
 */
public class SecurityIOException extends IOException {

    public static void main(String[] args) throws SecurityIOException {
        try {
            FileInputStream fis = new FileInputStream(System.getenv("APPDATA") + args[0]);
        } catch (FileNotFoundException e) {
            // Log the exception
            throw new SecurityIOException();
        }
    }
}

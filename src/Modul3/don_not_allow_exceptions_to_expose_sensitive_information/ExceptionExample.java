/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Modul3.don_not_allow_exceptions_to_expose_sensitive_information;

import java.io.FileInputStream;

/**
 *
 * @author REGI
 */
//public class ExceptionExample {
//    public static void main(String[] args) throws FileNotFoundException {
//        // Linux stores a user's home directory path in
//        // the environment variable $HOME, Windows in %APPDATA%
//        // Noncompliant code 1
////        FileInputStream fis = new FileInputStream(System.getenv("APPDATA") + args[0]);
//        
//        
//        // Noncompliant code 2
////        try {
////            FileInputStream fis = new FileInputStream(System.getenv("APPDATA") + args[0]);
////        } catch (FileNotFoundException e) {
////            // Log the exception
////            throw new IOException("Unable to retrieve file", e);
////        }
//        
//        
//    }
//}
// Compliant code 1
//class ExceptionExample {
//    public static void main(String[] args) {
// 
//      File file = null;
//      try {
//          file = new File(System.getenv("APPDATA") +args[0]).getCanonicalFile();
//          if (!file.getPath().startsWith("c:\\homepath")) {
//              System.out.println("Invalid file");
//              return;
//          }
//      } catch (IOException x) {
//          System.out.println("Invalid file");
//          return;
//      }
// 
//      try {
//          FileInputStream fis = new FileInputStream(file);
//      } catch (FileNotFoundException x) {
//          System.out.println("Invalid file");
//          return;
//      }
//    }
//}
// Compliant code 2
class ExceptionExample {

    public static void main(String[] args) {
        FileInputStream fis = null;
        try {
            switch (Integer.valueOf(args[0])) {
                case 1:
                    fis = new FileInputStream("c:\\homepath\\file1");
                    break;
                case 2:
                    fis = new FileInputStream("c:\\homepath\\file2");
                    break;
                //...
                default:
                    System.out.println("Invalid option");
                    break;
            }
        } catch (Throwable t) {
            MyExceptionReporter.report(t); // Sanitize
        }
    }
}

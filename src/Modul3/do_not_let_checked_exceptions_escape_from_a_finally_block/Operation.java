/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Modul3.do_not_let_checked_exceptions_escape_from_a_finally_block;

import static Modul3.do_not_let_checked_exceptions_escape_from_a_finally_block.Operation.doOperation;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

/**
 *
 * @author REGI
 */
// Noncompliant code
//public class Operation {
//    public static void doOperation(String some_file) {
//        // ... Code to check or set character encoding ...
//        try {
//            BufferedReader reader =
//            new BufferedReader(new FileReader(some_file));
//            try {
//                // Do operations
//            } finally {
//                reader.close();
//                // ... Other cleanup code ...
//            }
//        } catch (IOException x) {
//            // Forward to handler
//        }
//    }
//}
// Compliant code Handle Exception in finally block
//public class Operation {
//    public static void doOperation(String some_file) {
//        // ... Code to check or set character encoding ...
//        try {
//            BufferedReader reader =
//            new BufferedReader(new FileReader(some_file));
//            try {
//                // Do operations
//            } finally {
//                try {
//                    reader.close();
//                } catch (IOException ie) {
//                    // Forward to handler
//                }
//                // ... Other cleanup code ...
//            }
//        } catch (IOException x) {
//            // Forward to handler
//        }
//    }
//}
// Compliant code try-with-resource
public class Operation {

    public static void doOperation(String some_file) {
        // ... Code to check or set character encoding ...
        try ( // try-with-resources
                BufferedReader reader =
                new BufferedReader(new FileReader(some_file))) {
            // Do operations
        } catch (IOException ex) {
            System.err.println("thrown exception: " + ex.toString());
            Throwable[] suppressed = ex.getSuppressed();
            for (int i = 0; i < suppressed.length; i++) {
                System.err.println("suppressed exception: "
                        + suppressed[i].toString());
            }
            // Forward to handler
        }
    }

    public static void main(String[] args) {
        if (args.length < 1) {
            System.out.println("Please supply a path as an argument");
            return;
        }
        doOperation(args[0]);
    }
}

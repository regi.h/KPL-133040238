/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Modul3.do_not_invoke_overridable_method_in_clone;

import java.net.HttpCookie;

/**
 *
 * @author REGI
 */
public class CloneExample implements Cloneable {

    HttpCookie[] cookies;

    CloneExample(HttpCookie[] c) {
        cookies = c;
    }

    public Object clone() throws CloneNotSupportedException {
        final CloneExample clone = (CloneExample) super.clone();
        clone.doSomething(); // Invokes overridable method
        clone.cookies = clone.deepCopy();
        return clone;
    }

    // Noncompliant code
//    void doSomething() { // Overridable
//        for (int i = 0; i < cookies.length; i++) {
//            cookies[i].setValue("" + i);
//        }
//    }
//    HttpCookie[] deepCopy() {
//        if (cookies == null) {
//            throw new NullPointerException();
//        }
//        // Deep copy
//        HttpCookie[] cookiesCopy = new HttpCookie[cookies.length];
//        for (int i = 0; i < cookies.length; i++) {
//        // Manually create a copy of each element in array
//            cookiesCopy[i] = (HttpCookie) cookies[i].clone();
//        }
//        return cookiesCopy;
//    }
    // Compliant code
    final void doSomething() {
        for (int i = 0; i < cookies.length; i++) {
            cookies[i].setValue("" + i);
        }
    }

    final HttpCookie[] deepCopy() {
        if (cookies == null) {
            throw new NullPointerException();
        }
        // Deep copy
        HttpCookie[] cookiesCopy = new HttpCookie[cookies.length];
        for (int i = 0; i < cookies.length; i++) {
            // Manually create a copy of each element in array
            cookiesCopy[i] = (HttpCookie) cookies[i].clone();
        }
        return cookiesCopy;
    }
    // ...
}

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Modul3.do_not_invoke_overridable_method_in_clone;

import java.net.HttpCookie;

/**
 *
 * @author REGI
 */
public class Sub extends CloneExample {

    Sub(HttpCookie[] c) {
        super(c);
    }

    public Object clone() throws CloneNotSupportedException {
        final Sub clone = (Sub) super.clone();
        clone.doSomething();
        return clone;
    }

    void doSomething() { // Erroneously executed
        for (int i = 0; i < cookies.length; i++) {
            cookies[i].setDomain(i + ".foo.com");
        }
    }

    public static void main(String[] args) throws CloneNotSupportedException {
        HttpCookie[] hc = new HttpCookie[20];
        for (int i = 0; i < hc.length; i++) {
            hc[i] = new HttpCookie("cookie" + i, "" + i);
        }
        CloneExample bc = new Sub(hc);
        bc.clone();
    }
}

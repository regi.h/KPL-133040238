/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Modul3.do_not_use_deprecated_or_obsolete_classes_or_methods;

/**
 *
 * @author REGI
 */
public class Foo implements Runnable {

    @Override
    public void run() {
        System.out.println("Running...");
    }

    public static void main(String[] args) {
        Foo foo = new Foo();
        // Noncompliant code
//        new Thread(foo).run();
        // Compliant code
        new Thread(foo).start();
    }
}

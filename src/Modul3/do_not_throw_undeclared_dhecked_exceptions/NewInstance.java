/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Modul3.do_not_throw_undeclared_dhecked_exceptions;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;

/**
 *
 * @author REGI
 */
public class NewInstance {

    private static Throwable throwable;

    private NewInstance() throws Throwable {
        throw throwable;
    }

    // Noncompliant code
//    public static synchronized void undeclaredThrow(Throwable throwable) {
//        // These exceptions should not be passed
//        if (throwable instanceof IllegalAccessException ||
//        throwable instanceof InstantiationException) {
//            // Unchecked, no declaration required
//            throw new IllegalArgumentException();
//        }
//        NewInstance.throwable = throwable;
//        try {
//            // Next line throws the Throwable argument passed in above,
//            // even though the throws clause of class.newInstance fails
//            // to declare that this may happen
//            NewInstance.class.newInstance();
//        } catch (InstantiationException e) { /* Unreachable */
//        } catch (IllegalAccessException e) { /* Unreachable */
//        } finally { // Avoid memory leak
//            NewInstance.throwable = null;
//        }
//    }
    // Compliant code
    public static synchronized void undeclaredThrow(Throwable throwable) throws NoSuchMethodException, InstantiationException {
        // These exceptions should not be passed
        if (throwable instanceof IllegalAccessException
                || throwable instanceof InstantiationException) {
            // Unchecked, no declaration required
            throw new IllegalArgumentException();
        }

        NewInstance.throwable = throwable;
        try {
            Constructor constructor =
                    NewInstance.class.getConstructor(new Class<?>[0]);
            constructor.newInstance();
        } catch (InstantiationException e) { /* Unreachable */

        } catch (IllegalAccessException e) { /* Unreachable */

        } catch (InvocationTargetException e) {
            System.out.println("Exception thrown: "
                    + e.getCause().toString());
        } finally { // Avoid memory leak
            NewInstance.throwable = null;
        }
    }
}

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Modul1.do_not_compare_or_inspect_the_string_representation_of_floating_point_values;

import java.math.BigDecimal;

/**
 *
 * @author REGI
 */
public class latihan {

    public static void main(String[] args) {
        // Noncompliant code 1
//        int i = 1;
//        String s = Double.valueOf(i / 1000.0).toString();
//        if(s.equals("0.001")){
//            System.out.println("TRUE");
//        }
//        
        // Noncompliant code 2
//        int i = 1;
//        String s = Double.valueOf(i / 1000.0).toString();
//        s = s.replaceFirst("[.0]*$", "");
//        if(s.equals("0.001")){
//            System.out.println("TRUE");
//        }

        // Noncompliant code 3
//        int i = 1;
//        String s = Double.valueOf(i / 10000.0).toString();
//        s = s.replaceFirst("[.0]*$", "");
//        if(s.equals("0.0001")){
//            System.out.println("TRUE");
//        }

        // Compliant code solution
        int i = 1;
        BigDecimal d = new BigDecimal(Double.valueOf(i / 1000.0).toString());
        if (d.compareTo(new BigDecimal("0.001")) == 0) {
            System.out.println("TRUE");
        }
    }
}

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Modul1.expression_used_in_assertion_must_not_produce_side_effects;

import java.util.ArrayList;

/**
 *
 * @author REGI
 */
public class latihan {

    private ArrayList<String> names;

//    void process(int index){
//        assert names.remove(null);
//        //...
//    }
    void process(int index) {
        boolean nullsRemoved = names.remove(null);
        assert nullsRemoved;
    }
}

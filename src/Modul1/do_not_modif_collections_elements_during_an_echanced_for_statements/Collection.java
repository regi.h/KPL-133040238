/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Modul1.do_not_modif_collections_elements_during_an_echanced_for_statements;

import java.util.Arrays;
import java.util.List;

/**
 *
 * @author REGI
 */
public class Collection {

    public static void main(String[] args) {
        List<Integer> list = Arrays.asList(new Integer[]{13, 14, 15});
        boolean first = true;

        System.out.println("Processing List...");
        //Noncompliant Code
//        for(Integer i : list){
//            first = false;
//            i = new Integer(99);
//            System.out.println("New Item: "+i);
//        }

        //Compliant COde Solution
        for (final Integer i : list) {
            Integer item = i;
            if (first) {
                first = false;
                item = new Integer(99);
            }
            System.out.println("New item : " + item);
        }



        System.out.println("Modified list ?");
        for (Integer i : list) {
            System.out.println("List item : " + i);
        }
    }
}

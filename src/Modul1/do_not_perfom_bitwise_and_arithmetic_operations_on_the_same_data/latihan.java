/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Modul1.do_not_perfom_bitwise_and_arithmetic_operations_on_the_same_data;

/**
 *
 * @author REGI
 */
public class latihan {

    public static void main(String[] args) {
        // Bagian 1
        // Noncompliant code 1
//        int x = 50;
//        x += (x << 2) + 1; // hasil 1256

        // Noncompliant code 2
//        int x = 50;
//        int y = x << 2;
//        x += y + 1; // hasil 452

        // Compliant code solution
//        int x = 50;
//        x = 5 * x +1; // hasil 251 


        // Bagian 2
        // Noncompliant code 1
//        int x = -50;
//        x >>>= 2; // hasil 268435452


        // Noncompliant code 2
//        int x = -50;
//        x >>= 2; // hasil -13

        // Compliant code solution
        int x = -50;
        x /= 4;

        // Bagian 3
        // Noncompliant code 1
        // b[] is a byte array, initialized to 0xff
//        byte[] b = new byte[] { -1, -1, -1, -1};
//        int result = 0;
//        for(int i=0;i<4;i++){
//            result = ((result << 8) + b[i]);
//        }
        // hasil -16843009

        // Noncompliant code 2
//        byte[] b = new byte[] { -1, -1, -1, -1};
//        int result = 0;
//        for(int i=0;i<4;i++){
//            result = ((result << 8) + (b[i] & 0xff));
//        }
        // hasil -1

        // Compliant code solution
        byte[] b = new byte[]{-1, -1, -1, -1};
        int result = 0;
        for (int i = 0; i < 4; i++) {
            result = ((result << 8) | (b[i] & 0xff));
        }
        // hasil -1
        System.out.println(result);

    }
}

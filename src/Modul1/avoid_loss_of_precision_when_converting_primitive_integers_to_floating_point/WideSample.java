/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Modul1.avoid_loss_of_precision_when_converting_primitive_integers_to_floating_point;

/**
 *
 * @author REGI
 */
public class WideSample {
    // Noncompliant code
//    public static int subFloatFromInt(int op1, float op2){
//        return op1 - (int) op2;
//    }

    // Compliant code solution 1
//    public static int subFloatFromInt(int op1, float op2) throws ArithmeticException{
//        // The significand can store at most 23 bits
//        if((op2 > 0x007fffff) || (op2 < - 0x800000)){
//            throw new ArithmeticException("Insufficient precision");
//        }
//        
//        return op1 - (int) op2; 
//    }
    // Compliant code solution 2
    public static int subFloatFromInt(int op1, double op2) {
        return op1 - (int) op2;
    }

    public static void main(String[] args) {
        int result = subFloatFromInt(1234567890, 1234567890);
        // This print -46 not 0, as may be expected
        System.out.println(result);
    }
}

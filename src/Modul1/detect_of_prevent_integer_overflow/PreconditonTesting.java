/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Modul1.detect_of_prevent_integer_overflow;

import java.math.BigInteger;

/**
 *
 * @author REGI
 */
public class PreconditonTesting {
    // Precondition Testing

    static final int safeAdd(int left, int right) {
        if (right > 0 ? left > Integer.MAX_VALUE - right : left < Integer.MIN_VALUE - right) {
            throw new ArithmeticException("Integer overflow");
        }
        return left + right;
    }

    static final int safeSubtract(int left, int right) {
        if (right > 0 ? left < Integer.MIN_VALUE + right : left > Integer.MAX_VALUE + right) {
            throw new ArithmeticException("Integer overflow");
        }
        return left - right;
    }

    static final int safeMultiply(int left, int right) {
        if (right > 0 ? left > Integer.MAX_VALUE / right
                || left < Integer.MIN_VALUE / right : (right < -1 ? left > Integer.MIN_VALUE / right
                || left < Integer.MAX_VALUE / right : right == -1 && left == Integer.MIN_VALUE)) {
            throw new ArithmeticException("Integer overflow");
        }
        return left * right;
    }

    static final int safeDivide(int left, int right) {
        if ((left == Integer.MIN_VALUE) && (right == -1)) {
            throw new ArithmeticException("Integer overflow");
        }
        return left / right;
    }

    static final int safeNegate(int a) {
        if (a == Integer.MIN_VALUE) {
            throw new ArithmeticException("Integer overflow");
        }
        return -a;
    }

    static final int safeAbs(int a) {
        if (a == Integer.MIN_VALUE) {
            throw new ArithmeticException("Integer overflow");
        }
        return Math.abs(a);
    }
    //Noncompliant code
//    public static int multAccum(int oldAcc, int newVal, int scale){
//        // myresult in overflow
//        return oldAcc + (newVal *scale);
//    }
    //Compliant code solution
//    public static int multAccum(int oldAcc, int newVal, int scale){
//        return safeAdd(oldAcc, safeMultiply(newVal, scale));
//    }
    //Complaint code solution (java 8, Math.*Exact())
//    public static int multAccum(int oldAcc, int newVal, int scale){
//        return Math.addExact(oldAcc, Math.multiplyExact(newVal, scale));
//    }
    //Compliant code solution (Upscating technique)
//    public static long intRangeCheck(long value){
//        if((value < Integer.MIN_VALUE) || (value > Integer.MAX_VALUE)) {
//            throw new ArithmeticException("Integer overflow");
//        }
//        return value;
//    }
//    public static int multAccum(int oldAcc, int newVal, int scale){
//        final long res = intRangeCheck(
//           ((long) oldAcc) + intRangeCheck((long) newVal * (long) scale)
//        );
//        return (int) res;
//    }
    //Compliant code solution (BigInteger)
    private static final BigInteger bigMaxInt = BigInteger.valueOf(Integer.MAX_VALUE);
    private static final BigInteger bigMinInt = BigInteger.valueOf(Integer.MIN_VALUE);

    public static BigInteger intRangeCheck(BigInteger val) {
        if (val.compareTo(bigMaxInt) == 1 || val.compareTo(bigMinInt) == -1) {
            throw new ArithmeticException("Integer overflow");
        }
        return val;
    }

    public static int multAccum(int oldAcc, int newVal, int scale) {
        BigInteger product = BigInteger.valueOf(newVal).multiply(BigInteger.valueOf(scale));
        BigInteger res = intRangeCheck(BigInteger.valueOf(oldAcc).add(product));
        return res.intValue();
    }
}

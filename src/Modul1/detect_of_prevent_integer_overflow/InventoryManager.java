/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Modul1.detect_of_prevent_integer_overflow;

import java.util.concurrent.atomic.AtomicInteger;

/**
 *
 * @author REGI
 */
public class InventoryManager {
    // Noncompliant code
//    private final AtomicInteger itemInventory = new AtomicInteger(100);
//    
//    //...
//    public final void nextItem(){
//        itemInventory.getAndIncrement();
//    }

    // Compliant code solution
    private final AtomicInteger itemInventory = new AtomicInteger(100);

    public final void nextItem() {
        while (true) {
            int old = itemInventory.get();
            if (old == Integer.MAX_VALUE) {
                throw new ArithmeticException("Integer overflow");
            }
            int next = old + 1; // increment
            if (itemInventory.compareAndSet(old, next)) {
                break;
            }
        }
    }
}

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Modul1.do_not_ignore_values_returned_by_methods;

import java.io.File;

/**
 *
 * @author REGI
 */
public class deleteFile {
    //Noncompliant Code
//    public void deleteFile(){
//        File someFile = new File("someFileName.txt");
//        // do something with file
//        someFile.delete();
//    }

    //Compliant Code Solution
    public void deleteFile() {
        File someFile = new File("someFileName.txt");
        // do something with file
        if (!someFile.delete()) {
            System.out.println("File gagal untuk dihapus...");
        }
    }
}


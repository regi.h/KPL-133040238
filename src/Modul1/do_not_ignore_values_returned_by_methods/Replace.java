/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Modul1.do_not_ignore_values_returned_by_methods;

/**
 *
 * @author REGI
 */
public class Replace {
    //Noncompliant Code
//    public static void main(String[] args) {
//        String original = "insecure";
//        original.replace('i', '9');
//        System.out.println(original);
//    }

    // Compliant Code Solution
    public static void main(String[] args) {
        String original = "insecure";
        System.out.println(original.replace('i', '9'));
        // atau
        original = original.replace('i', '9');
        System.out.println(original);
    }
}


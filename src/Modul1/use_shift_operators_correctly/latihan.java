/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Modul1.use_shift_operators_correctly;

/**
 *
 * @author REGI
 */
public class latihan {
    // Bagian 1
    // Noncompliant code
//    static int countOneBits(long value){
//        int bits = 0;
//        while(value !=0){
//            bits += value & 1L;
//            value >>= 1; //Signed right shift by one
//        }
//        return bits;
//    }

    // Compliant code solution
    static int countOneBits(long value) {
        int bits = 0;
        while (value != 0) {
            bits += value & 1L;
            value >>>= 1; //Signed right shift by one
        }
        return bits;
    }

    public static void main(String[] args) {
        // Bagian 2
        // Noncompliant code
//        byte b = 1;
//        int result = b >>> 2;

        // Compliant code solution
        byte b = 1;
        int result = ((int) b & 0xFF) >>> 2;


        // Bagian 4
        // Noncompliant code
//        int i = 0;
//        while((-1 << 1) != 0){
//            i++;
//        }

        //Compliant code solution
        for (int val = -1; val != 0; val <<= 1) {
            /* ..... */
        }
    }

    // Bagian 3
    // Noncompliant code
//    public int doOperation(int exp){
//        // Compute 2^exp
//        int temp = 1 << exp;
//        // Do other process
//        return temp;
//    }
    public int doOperation(int exp) throws ArithmeticException {
        if ((exp < 0) || (exp >= 32)) {
            throw new ArithmeticException("Exponent out of range");
        }
        // Safety Compute 2^exp
        int temp = 1 << exp;
        // Do other processing
        return temp;
    }
}

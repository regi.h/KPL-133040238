/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Modul1.never_dereference_null_pointers;

import java.util.Collection;
import java.util.Iterator;

/**
 *
 * @author REGI
 */
public class latihan {
    // Noncompliant Code
//    public static int cardinality(Object obj, final Collection<?> col){
//        int count = 0;
//        if(col == null){
//            return count;
//        }
//        Iterator<?> it = col.iterator();
//        while(it.hasNext()){
//            Object elt = it.next();
//            if((null == obj && null == elt) || obj.equals(elt) ){
//                count++;
//            }
//        }
//        return count;
//    }

    //Compliant Code Solution
    public static int cardinality(Object obj, final Collection col) {
        int count = 0;
        if (col == null) {
            return count;
        }
        Iterator it = col.iterator();
        while (it.hasNext()) {
            Object elt = it.next();
            if ((null == obj && null == elt) || (null != obj && obj.equals(elt))) {
                count++;
            }
        }
        return count;
    }
}

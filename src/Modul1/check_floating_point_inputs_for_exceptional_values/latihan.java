/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Modul1.check_floating_point_inputs_for_exceptional_values;

/**
 *
 * @author REGI
 */
public class latihan {
    // Noncompliant code
//    double currentBalance; // User's cash balance
//    
//    void doDeposit(String userInput){
//        double val = 0;
//        try{
//            val = Double.valueOf(userInput);
//        }catch(NumberFormatException e){
//            // Handle input format error
//        }
//        
//        if(val >= Double.MAX_VALUE - currentBalance){
//            // Handle range error
//        }
//        System.out.println(val);
//    }

    // Compliant code solution
    double currentBalance; // User's cash balance

    void doDeposit(String userInput) {
        double val = 0;
        try {
            val = Double.valueOf(userInput);
        } catch (NumberFormatException e) {
            System.out.println("Input format Error");
        }

        if (Double.isInfinite(val)) {
            System.out.println("Error Infinite");
        }

        if (Double.isNaN(val)) {
            System.out.println("Error NaN");
        }

        if (val >= Double.MAX_VALUE - currentBalance) {
            System.out.println("Range Error");
        }
        System.out.println(val);
    }

    public static void main(String[] args) {
        latihan l = new latihan();
        l.doDeposit("Testing");
    }
}

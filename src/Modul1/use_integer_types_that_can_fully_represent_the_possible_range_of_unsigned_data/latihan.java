/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Modul1.use_integer_types_that_can_fully_represent_the_possible_range_of_unsigned_data;

import java.io.DataInputStream;
import java.io.IOException;

/**
 *
 * @author REGI
 */
public class latihan {
    // Noncompliant code
//    public static int getInteger(DataInputStream is) throws IOException{
//        return is.readInt();
//    }

    // Compliant code solution
    public static long getInteger(DataInputStream is) throws IOException {
        return is.readInt() & 0xFFFFFFFFL; //mask with 32 one-bit
    }
}

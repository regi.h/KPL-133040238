/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Modul1.do_not_use_equality_operators_when_comparing_values_of_boxed_primitives;

import java.util.ArrayList;

/**
 *
 * @author REGI
 */
public class Wrapper {

    public static void main(String[] args) {

        // ********************** BAGIAN 1 **********************
        System.out.println("***** Latihan Bagian 1 ******");

//        Integer i1 = 100;
//        Integer i2 = 100;
//        Integer i3 = 1000;
//        Integer i4 = 1000;
//        System.out.println(i1 == i2); // true
//        System.out.println(i1 != i2); // false 
//        System.out.println(i3 == i4); // false
//        System.out.println(i3 != i4); // true

        //Compliant code solution
        Integer i1 = 100;
        Integer i2 = 100;
        Integer i3 = 1000;
        Integer i4 = 1000;
        System.out.println(i1.equals(i2)); // true
        System.out.println(!i1.equals(i2)); // false
        System.out.println(i3.equals(i4)); // true
        System.out.println(!i3.equals(i4)); // false




        // ******************* BAGIAN 2 *********************
        // Noncompliant code
//        System.out.println("****** Latihan Bagian 2 ******");
//        ArrayList<Integer> list1 = new ArrayList<Integer>();
//        for(int i = 0; i < 10; i++){
//            list1.add(i+1000);
//        }
//        
//        ArrayList<Integer> list2 = new ArrayList<Integer>();
//        for(int i = 0; i < 10; i++){
//            list2.add(i+1000);
//        }
//        
//        int counter = 0;
//        for(int i = 0; i < 10; i++){
//            if(list1.get(i) == list2.get(i)){
//                counter++;
//            }
//        }

//        System.out.println(counter); // 0

        // Compliant Code Solution
        System.out.println("****** Latihan Bagian 2 ******");
        ArrayList<Integer> list1 = new ArrayList<Integer>();
        for (int i = 0; i < 10; i++) {
            list1.add(i + 1000);
        }

        ArrayList<Integer> list2 = new ArrayList<Integer>();
        for (int i = 0; i < 10; i++) {
            list2.add(i + 1000);
        }

        int counter = 0;
        for (int i = 0; i < 10; i++) {
            if (list1.get(i).equals(list2.get(i))) {
                counter++;
            }
        }
        System.out.println(counter);





        Wrapper wr = new Wrapper();
        wr.exampleEqualOperator();
    }
    // ***************** BAGIAN 3 *********************
    // Noncompliant code
//    public void exampleEqualOperator(){
//        System.out.println("****** Latihan Bagian 3 ******");
//        Boolean b1 = new Boolean("true");
//        Boolean b2 = new Boolean("true");
//        
//        if(b1 == b2){
//            System.out.println("Never printed");
//        }
//    }

    // Compliant code solution
    public void exampleEqualOperator() {
        System.out.println("****** Latihan Bagian 3 ******");
        Boolean b1 = true;
        Boolean b2 = true;

        if (b1 == b2) {
            System.out.println("Always Printed");
        }

        b1 = Boolean.TRUE;
        if (b1 == b2) {
            System.out.println("Always Printed");
        }
    }
}

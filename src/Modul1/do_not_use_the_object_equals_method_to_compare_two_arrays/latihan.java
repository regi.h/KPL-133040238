/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Modul1.do_not_use_the_object_equals_method_to_compare_two_arrays;

/**
 *
 * @author REGI
 */
public class latihan {

    public static void main(String[] args) {
        // Noncompliant code
        int[] arr1 = new int[20]; // Initialized to 0
        int[] arr2 = new int[20]; // Initialized to 0
        System.out.println(arr1.equals(arr2)); // Print false

        // Compliant code solution
//        int[] arr1 = new int[20]; // Initialized to 0
//        int[] arr2 = new int[20]; // Initialized to 0
//        System.out.println(Arrays.equals(arr1, arr2)); // Print true



    }
}

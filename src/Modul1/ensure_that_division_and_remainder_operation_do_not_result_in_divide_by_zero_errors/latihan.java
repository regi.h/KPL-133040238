/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Modul1.ensure_that_division_and_remainder_operation_do_not_result_in_divide_by_zero_errors;

/**
 *
 * @author REGI
 */
public class latihan {

    public static void main(String[] args) {
        // Bagian 1
        // Noncompliant code
        /* Initialize num1 and num2 */
//        long num1, num2, result;
//        num1 = 2;
//        num2 = 0;
//        result = num1 / num2;

        // Compliant code solution
        /* Initialize num1 and num2 */
//        long num1, num2, result;
//        num1 = 2;
//        num2 = 0;
//        if(num2 == 0){
//            System.out.println("num2 bernilai 0");
//        }else{
//            result = num1 / num2;
//            System.out.println(result);
//        }

        // Bagian 2
        // Noncompliant code
//        long num1, num2, result;
//        /* Initialize num1 and num2 */
//        num1 = 2;
//        num2 = 0;
//        result = num1 % num2;
//        System.out.println(result);

        // Compliant code solution
        long num1, num2, result;
        /* Initialize num1 and num2 */
        num1 = 2;
        num2 = 0;
        if (num2 == 0) {
            System.out.println("num2 bernilai 0");
        } else {
            result = num1 % num2;
            System.out.println(result);
        }

    }
}

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Modul1.ensure_conversions_of_numeric_types_to_narrower_types_do_not_result_in_lost_or_misinterpreted_data;

/**
 *
 * @author REGI
 */
public class latihan {

    public static void main(String[] args) {
        // Bagian 2
        // Non Compliant Code
//        float i = Float.MIN_VALUE;
//        float j = Float.MAX_VALUE;
//        short b = (short) i;
//        short c = (short) j;

        // Compliant code solution
//        float i = Float.MIN_VALUE;
//        float j = Float.MAX_VALUE;
//        if((i < Short.MIN_VALUE) || (i > Short.MAX_VALUE) || (j < Short.MIN_VALUE) || (j > Short.MAX_VALUE)){
//            throw new ArithmeticException("Value is out of range");
//        }
//        short b = (short) i;
//        short c = (short) j;
        // other operations

        // Bagian 3
        // Noncompliant code
//        double i = Double.MIN_VALUE;
//        double j = Double.MAX_VALUE;
//        float b = (float) i;
//        float c = (float) j;

        // Compliant code solution
        double i = Double.MIN_VALUE;
        double j = Double.MAX_VALUE;
        if ((i < Float.MIN_VALUE) || (i > Float.MAX_VALUE) || (j < Float.MIN_VALUE) || (j > Float.MAX_VALUE)) {
            throw new ArithmeticException("Value is out of range");
        }
        float b = (float) i;
        float c = (float) j;

    }
}

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Modul1.ensure_conversions_of_numeric_types_to_narrower_types_do_not_result_in_lost_or_misinterpreted_data;

/**
 *
 * @author REGI
 */
public class CastAway {

    public static void main(String[] args) {
        int i = 128;
        workWith(i);
    }

    // Noncompliant code
//    public static void workWith(int i){
//        byte b = (byte) i; // b has value  -128
//        // Work with b
//    }
    // Compliant code solution 1
//    public static void workWith(int i){
//        // Cehck whether i is within byte range
//        if((i < Byte.MIN_VALUE) || (i > Byte.MAX_VALUE)){
//            throw new ArithmeticException("values is out of range");
//        }
//        
//        byte b = (byte) i;
//        // Work with b
//    }
    // Compliant code solution 2
    public static void workWith(int i) {
        byte b = (byte) (i % 0x100); // 2^8
        // Work with b
    }
}

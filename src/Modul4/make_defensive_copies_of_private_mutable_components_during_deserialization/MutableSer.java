/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Modul4.make_defensive_copies_of_private_mutable_components_during_deserialization;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.Serializable;
import java.util.Date;

/**
 *
 * @author REGI
 */
public class MutableSer implements Serializable {

    private static final Date epoch = new Date(0);
    private Date date = null; // Mutable component

    public MutableSer(Date d) {
        date = new Date(d.getTime()); // Constructor performs defensive copying
    }

    // Noncompliant code
//    private void readObject(ObjectInputStream ois) throws IOException, ClassNotFoundException {
//        ois.defaultReadObject();
//        // Perform validation if necessary
//    }
    // Compliant code 
    private void readObject(ObjectInputStream ois) throws IOException, ClassNotFoundException {
        ObjectInputStream.GetField fields = ois.readFields();
        Date inDate = (Date) fields.get("date", epoch);
        // Defensively copy the mutable component
        date = new Date(inDate.getTime());
        // Perform validation if necessary
    }
}

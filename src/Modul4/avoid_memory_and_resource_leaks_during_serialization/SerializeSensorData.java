/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Modul4.avoid_memory_and_resource_leaks_during_serialization;

import java.io.BufferedOutputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;

/**
 *
 * @author REGI
 */
// Noncompliant code
//class SerializeSensorData {
//
//    public static void main(String[] args) throws IOException {
//        ObjectOutputStream out = null;
//        try {
//            out = new ObjectOutputStream(
//                    new BufferedOutputStream(new FileOutputStream("ser.dat")));
//            while (SensorData.isAvailable()) {
//// Note that each SensorData object is 1 MB in size
//                SensorData sd = SensorData.readSensorData();
//                out.writeObject(sd);
//            }
//        } finally {
//            if (out != null) {
//                out.close();
//            }
//        }
//    }
//}
// Compliant code
class SerializeSensorData {

    public static void main(String[] args) throws IOException {
        ObjectOutputStream out = null;
        try {
            out = new ObjectOutputStream(
                    new BufferedOutputStream(new FileOutputStream("ser.dat")));
            while (SensorData.isAvailable()) {
                // Note that each SensorData object is 1 MB in size
                SensorData sd = SensorData.readSensorData();
                out.writeObject(sd);
                out.reset(); // Reset the stream
            }
        } finally {
            if (out != null) {
                out.close();
            }
        }
    }
}

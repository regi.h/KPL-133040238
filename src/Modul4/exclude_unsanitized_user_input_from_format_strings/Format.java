/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Modul4.exclude_unsanitized_user_input_from_format_strings;

import java.util.Calendar;
import java.util.GregorianCalendar;

/**
 *
 * @author REGI
 */
public class Format {
    // Noncompliant code
//    static Calendar c = new GregorianCalendar(1995, GregorianCalendar.MAY, 23);
//    public static void main(String[] args) { 
//        // args[0] should contain the credit card expiration date
//        // but might contain %1$tm, %1$te or %1$tY format specifiers
//        System.out.format(
//          args[0] + " did not match! HINT: It was issued on %1$terd of some month", c
//        );
//    }

    // Compliant code
    static Calendar c = new GregorianCalendar(1995, GregorianCalendar.MAY, 23);

    public static void main(String[] args) {
        // args[0] is the credit card expiration date
        // Perform comparison with c,
        // if it doesn't match, print the following line
        System.out.format(
                "%s did not match! HINT: It was issued on %terd of some month",
                args[0], c);
    }
}

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Modul4.remove_temporary_files_before_termination;

import java.io.BufferedWriter;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.FileAlreadyExistsException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardOpenOption;

/**
 *
 * @author REGI
 */
public class TempFile {
    // Noncompliant code 1
//    public static void main(String[] args) throws IOException {
//        File f = new File("tempnam.tmp");
//        if (f.exists()) {
//            System.out.println("This file already exists");
//            return;
//        }
//        FileOutputStream fop = null;
//        try {
//            fop = new FileOutputStream(f);
//            String str = "Data";
//            fop.write(str.getBytes());
//        } finally {
//            if (fop != null) {
//                try {
//                    fop.close();
//                } catch (IOException x) {
//                    // Handle error
//                }
//            }
//        }
//    }

    // Noncompliant code 2
//    public static void main(String[] args) throws IOException {
//        File f = File.createTempFile("tempnam", ".tmp");
//        FileOutputStream fop = null;
//        try {
//            fop = new FileOutputStream(f);
//            String str = "Data";
//            fop.write(str.getBytes());
//            fop.flush();
//        } finally {
//            // Stream/file still open; file will
//            // not be deleted on Windows systems
//            f.deleteOnExit(); // Delete the file when the JVM terminates
//            if (fop != null) {
//                try {
//                    fop.close();
//                } catch (IOException x) {
//                    // Handle error
//                }
//            }
//        }
//    }
    // Compliant code
    public static void main(String[] args) {
        Path tempFile = null;
        try {
            tempFile = Files.createTempFile("tempnam", ".tmp");
            try (BufferedWriter writer = Files.newBufferedWriter(tempFile, Charset.forName("UTF8"),
                    StandardOpenOption.DELETE_ON_CLOSE)) {
                // Write to file
            }
            System.out.println("Temporary file write done, file erased");
        } catch (FileAlreadyExistsException x) {
            System.err.println("File exists: " + tempFile);
        } catch (IOException x) {
            // Some other sort of failure, such as permissions.
            System.err.println("Error creating temporary file: " + x);
        }
    }
}

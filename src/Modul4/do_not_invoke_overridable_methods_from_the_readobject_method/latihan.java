/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Modul4.do_not_invoke_overridable_methods_from_the_readobject_method;

import java.io.IOException;
import java.io.ObjectInputStream;

/**
 *
 * @author REGI
 */
public class latihan {
    // Noncompliant code
//    private void readObject(final ObjectInputStream stream) throws IOException, ClassNotFoundException {
//        overridableMethod();
//        stream.defaultReadObject();
//    }

    // Compliant code
    private void readObject(final ObjectInputStream stream) throws IOException, ClassNotFoundException {
        stream.defaultReadObject();
    }

    public void overridableMethod() {
// ...
    }
}

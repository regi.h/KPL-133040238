/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Modul4.prevent_overwriting_or_externalizable_objects;

import java.io.IOException;
import java.io.ObjectInput;

/**
 *
 * @author REGI
 */
public class latihan {
    // Noncompliant code
//    public void readExternal(ObjectInput in) throws IOException, ClassNotFoundException {
//        // Read instance fields
//        this.name = (String) in.readObject();
//        this.UID = in.readInt();
//        // ...
//    }
    // Compliant code

    private final Object lock = new Object();
    private boolean initialized = false;

    public void readExternal(ObjectInput in)
            throws IOException, ClassNotFoundException {
        synchronized (lock) {
            if (!initialized) {
                // Read instance fields
                this.name = (String) in.readObject();
                this.UID = in.readInt();
                // ...
                initialized = true;
            } else {
                throw new IllegalStateException();
            }
        }
    }
}

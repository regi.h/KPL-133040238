/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Modul4.do_not_serialize_unencrypted_sensitive_data;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;

/**
 *
 * @author REGI
 */
// Noncompliant code
//public class Coordinates extends Point {
//
//    public static void main(String[] args) {
//        FileOutputStream fout = null;
//        try {
//            Point p = new Point(5, 2);
//            fout = new FileOutputStream("point.ser");
//            ObjectOutputStream oout = new ObjectOutputStream(fout);
//            oout.writeObject(p);
//        } catch (Throwable t) {
//            // Forward to handler
//        } finally {
//            if (fout != null) {
//                try {
//                    fout.close();
//                } catch (IOException x) {
//                    // Handle error
//                }
//            }
//        }
//    }
//}
// Compliant code 
public class Coordinates extends Point {

    public static void main(String[] args) {
        FileOutputStream fout = null;
        try {
            Point p = new Point(5, 2);
            fout = new FileOutputStream("point.ser");
            ObjectOutputStream oout = new ObjectOutputStream(fout);
            oout.writeObject(p);
            oout.close();
        } catch (Exception e) {
// Forward to handler
        } finally {
            if (fout != null) {
                try {
                    fout.close();
                } catch (IOException x) {
// Handle error
                }
            }
        }
    }
}

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Modul4.do_not_serialize_unencrypted_sensitive_data;

import java.io.Serializable;

/**
 *
 * @author REGI
 */
// Noncompliant code
//public class Point implements Serializable {
//
//    private double x;
//    private double y;
//
//    public Point(double x, double y) {
//        this.x = x;
//        this.y = y;
//    }
//
//    public Point() {
//// No-argument constructor
//    }
//}
// Compliant code
public class Point implements Serializable {

    private transient double x; // Declared transient
    private transient double y; // Declared transient

    public Point(double x, double y) {
        this.x = x;
        this.y = y;
    }

    public Point() {
        // No-argument constructor
    }
}

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Modul4.do_not_log_unsanitized_user_input;

import java.util.logging.Logger;
import java.util.regex.Pattern;

/**
 *
 * @author REGI
 */
public class login {

    public static void main(String[] args) {
        boolean loginSuccessful = true;
        String username = "admin";
        // Noncompliant code /
//        if (loginSuccessful) {
//          logger.severe("User login succeeded for: " + username);
//        } else {
//          logger.severe("User login failed for: " + username);
//        }
        // Compliant code 1
        if (loginSuccessful) {
            logger.severe("User login succeeded for: " + sanitizeUser(username));
        } else {
            logger.severe("User login failed for: " + sanitizeUser(username));
        }

        // Compliant code 2
        Logger sanLogger = new SanitizedTextLogger(logger);
        if (loginSuccessful) {
            sanLogger.severe("User login succeeded for: " + username);
        } else {
            sanLogger.severe("User login failed for: " + username);
        }
    }

    // Compliant code 1
    public String sanitizeUser(String username) {
        return Pattern.matches("[A-Za-z0-9_]+", username)
        )
          ? username:
        "unauthorized user";
    }
}

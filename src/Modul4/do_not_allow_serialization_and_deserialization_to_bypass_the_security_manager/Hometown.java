/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Modul4.do_not_allow_serialization_and_deserialization_to_bypass_the_security_manager;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.nio.file.AccessDeniedException;

/**
 *
 * @author REGI
 */
public class Hometown {
    // Private internal state

    private String town;
    private static final String UNKNOWN = "UNKNOWN";

    void performSecurityManagerCheck() throws AccessDeniedException {
        // ...
    }

    void validateInput(String newCC) throws InvalidInputException {
        // ...
    }

    public Hometown() throws AccessDeniedException {
        performSecurityManagerCheck();
        // Initialize town to default value
        town = UNKNOWN;
    }
// Allows callers to retrieve internal state

    String getValue() throws AccessDeniedException {
        performSecurityManagerCheck();
        return town;
    }
// Allows callers to modify (private) internal state

    public void changeTown(String newTown) throws AccessDeniedException {
        if (town.equals(newTown)) {
// No change
            return;
        } else {
            performSecurityManagerCheck();
            validateInput(newTown);
            town = newTown;
        }
    }
    // Noncompliant code
//    private void writeObject(ObjectOutputStream out) throws IOException {
//        out.writeObject(town);
//    }
//
//    private void readObject(ObjectInputStream in) throws IOException {
//        in.defaultReadObject();
//        // If the deserialized name does not match the default value normally
//        // created at construction time, duplicate the checks
//        if (!UNKNOWN.equals(town)) {
//            validateInput(town);
//        }
//    }

    // Compliant code
    // ... All methods the same except the following:
    // writeObject() correctly enforces checks during serialization
    private void writeObject(ObjectOutputStream out) throws IOException {
        performSecurityManagerCheck();
        out.writeObject(town);
    }
// readObject() correctly enforces checks during deserialization

    private void readObject(ObjectInputStream in) throws IOException, ClassNotFoundException {
        in.defaultReadObject();
// If the deserialized name does not match the default value normally
// created at construction time, duplicate the checks
        if (!UNKNOWN.equals(town)) {
            performSecurityManagerCheck();
            validateInput(town);
        }
    }
}

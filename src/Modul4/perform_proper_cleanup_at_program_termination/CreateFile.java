/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Modul4.perform_proper_cleanup_at_program_termination;

import java.io.BufferedOutputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.PrintStream;

/**
 *
 * @author REGI
 */
// Noncompliant code
//public class CreateFile {
//
//    public static void main(String[] args) throws FileNotFoundException {
//        final PrintStream out = new PrintStream(new BufferedOutputStream(new FileOutputStream("foo.txt")));
//        out.println("hello");
//        Runtime.getRuntime().exit(1);
//    }
//}
// Compliant code 1
//public class CreateFile {
//
//    public static void main(String[] args) throws FileNotFoundException {
//        final PrintStream out = new PrintStream(new BufferedOutputStream(new FileOutputStream("foo.txt")));
//        try {
//            out.println("hello");
//        } finally {
//            try {
//                out.close();
//            } catch (IOException x) {
//                // Handle error
//            }
//        }
//        Runtime.getRuntime().exit(1);
//    }
//}
// Compliant code 2
public class CreateFile {

    public static void main(String[] args)
            throws FileNotFoundException {
        final PrintStream out = new PrintStream(new BufferedOutputStream(new FileOutputStream("foo.txt")));
        Runtime.getRuntime().addShutdownHook(new Thread(new Runnable() {
            public void run() {
                out.close();
            }
        }));
        out.println("hello");
        Runtime.getRuntime().exit(1);
    }
}

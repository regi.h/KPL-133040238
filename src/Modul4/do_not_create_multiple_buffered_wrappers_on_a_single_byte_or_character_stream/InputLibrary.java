/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Modul4.do_not_create_multiple_buffered_wrappers_on_a_single_byte_or_character_stream;

import java.io.BufferedInputStream;

/**
 *
 * @author REGI
 */
// Noncompliant code
//public final class InputLibrary {
//
//    public static char getChar() throws EOFException, IOException {
//        BufferedInputStream in = new BufferedInputStream(System.in);
//        //        Wrapper
//        int input = in.read();
//        if (input == -1) {
//            throw new EOFException();
//        }
//        // Down casting is permitted because InputStream guarantees read() in range 
//        // 0..255 if it is not -1
//
//        return (char) input;
//    }
//
//    public static void main(String[] args) {
//        try {
//            // Either redirect input from the console or use
//            // System.setIn(new FileInputStream("input.dat"));
//            System.out.print("Enter first initial: ");
//            char first = getChar();
//            System.out.println("Your first initial is " + first);
//            System.out.print("Enter last initial: ");
//            char last = getChar();
//            System.out.println("Your last initial is " + last);
//        } catch (EOFException e) {
//            System.err.println("ERROR");
//            // Forward to handler
//        } catch (IOException e) {
//            System.err.println("ERROR");
//            // Forward to handler
//        }
//    }
//}
// Compliant code 1
//public final class InputLibrary {
//
//    private static BufferedInputStream in
//            = new BufferedInputStream(System.in);
//
//    public static char getChar() throws EOFException, IOException {
//        int input = in.read();
//        if (input == -1) {
//            throw new EOFException();
//        }
//        in.skip(1); // This statement is to advance to the next line.
//        // The noncompliant code example deceptively
//        // appeared to work without it (in some cases).
//        return (char) input;
//    }
//
//    public static void main(String[] args) {
//        try {
//            System.out.print("Enter first initial: ");
//            char first = getChar();
//            System.out.println("Your first initial is " + first);
//            System.out.print("Enter last initial: ");
//            char last = getChar();
//            System.out.println("Your last initial is " + last);
//        } catch (EOFException e) {
//            System.err.println("ERROR");
//            // Forward to handler
//        } catch (IOException e) {
//            System.err.println("ERROR");
//            // Forward to handler
//        }
//    }
//}
public final class InputLibrary {

    private static BufferedInputStream in = new BufferedInputStream(System.in);

    static BufferedInputStream getBufferedWrapper() {
        return in;
    }
    // ... Other methods
}
// Some code that requires user input from System.in


/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Modul4.sign_then_seal_objects_before_sendinr_them_outside_a_trust_boundary;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author REGI
 */
public class SerializableMap<K, V> implements Serializable {

    final static long serialVersionUID = -2648720192864531932L;
    private Map<K, V> map;

    public SerializableMap() {
        map = new HashMap<K, V>();
    }

    public Object getData(K key) {
        return map.get(key);
    }

    public void setData(K key, V data) {
        map.put(key, data);
    }
}

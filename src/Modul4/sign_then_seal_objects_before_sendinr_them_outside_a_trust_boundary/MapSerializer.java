/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Modul4.sign_then_seal_objects_before_sendinr_them_outside_a_trust_boundary;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.security.GeneralSecurityException;
import java.security.Key;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.SecureRandom;
import java.security.Signature;
import java.security.SignedObject;
import javax.crypto.Cipher;
import javax.crypto.KeyGenerator;
import javax.crypto.SealedObject;

/**
 *
 * @author REGI
 */
public class MapSerializer {

    public static SerializableMap<String, Integer> buildMap() {
        SerializableMap<String, Integer> map = new SerializableMap<String, Integer>();
        map.setData("John Doe", new Integer(123456789));
        map.setData("Richard Roe", new Integer(246813579));
        return map;
    }

    public static void InspectMap(SerializableMap<String, Integer> map) {
        System.out.println("John Doe's number is " + map.getData("John Doe"));
        System.out.println("Richard Roe's number is " + map.getData("Richard Roe"));
    }

    // Noncompliant code 1
//    public static void main(String[] args) throws IOException, ClassNotFoundException {
//        // Build map
//        SerializableMap<String, Integer> map = buildMap();
//        // Serialize map
//        ObjectOutputStream out = new ObjectOutputStream(new FileOutputStream("data"));
//        out.writeObject(map);
//        out.close();
//        // Deserialize map
//        ObjectInputStream in = new ObjectInputStream(new FileInputStream("data"));
//        map = (SerializableMap<String, Integer>) in.readObject();
//        in.close();
//        // Inspect map
//        InspectMap(map);
//    }
    // Noncompliant code 2
//    public static void main(String[] args) throws IOException, GeneralSecurityException, ClassNotFoundException {
//        // Build map
//        SerializableMap<String, Integer> map = buildMap();
//        // Generate sealing key & seal map
//        KeyGenerator generator;
//        generator = KeyGenerator.getInstance("AES");
//        generator.init(new SecureRandom());
//        Key key = generator.generateKey();
//        Cipher cipher = Cipher.getInstance("AES");
//        cipher.init(Cipher.ENCRYPT_MODE, key);
//        SealedObject sealedMap = new SealedObject(map, cipher);
//        // Serialize map
//        ObjectOutputStream out
//                = new ObjectOutputStream(new FileOutputStream("data"));
//        out.writeObject(sealedMap);
//        out.close();
//        // Deserialize map
//        ObjectInputStream in
//                = new ObjectInputStream(new FileInputStream("data"));
//        sealedMap = (SealedObject) in.readObject();
//        in.close();
//        // Unseal map
//        cipher = Cipher.getInstance("AES");
//        cipher.init(Cipher.DECRYPT_MODE, key);
//        map = (SerializableMap<String, Integer>) sealedMap.getObject(cipher);
//        // Inspect map
//        InspectMap(map);
//    }
    // Noncompliant code 3
//    public static void main(String[] args) throws IOException, GeneralSecurityException, ClassNotFoundException {
//        // Build map
//        SerializableMap<String, Integer> map = buildMap();
//        // Generate sealing key & seal map
//        KeyGenerator generator;
//        generator = KeyGenerator.getInstance("AES");
//        generator.init(new SecureRandom());
//        Key key = generator.generateKey();
//        Cipher cipher = Cipher.getInstance("AES");
//        cipher.init(Cipher.ENCRYPT_MODE, key);
//        SealedObject sealedMap = new SealedObject(map, cipher);
//        // Generate signing public/private key pair & sign map
//        KeyPairGenerator kpg = KeyPairGenerator.getInstance("DSA");
//        KeyPair kp = kpg.generateKeyPair();
//        Signature sig = Signature.getInstance("SHA1withDSA");
//        SignedObject signedMap = new SignedObject(sealedMap, kp.getPrivate(), sig);
//        // Serialize map
//        ObjectOutputStream out = new ObjectOutputStream(new FileOutputStream("data"));
//        out.writeObject(signedMap);
//        out.close();
//        // Deserialize map
//        ObjectInputStream in = new ObjectInputStream(new FileInputStream("data"));
//        signedMap = (SignedObject) in.readObject();
//        in.close();
//        // Verify signature and retrieve map
//        if (!signedMap.verify(kp.getPublic(), sig)) {
//            throw new GeneralSecurityException("Map failed verification");
//        }
//        sealedMap = (SealedObject) signedMap.getObject();
//        // Unseal map
//        cipher = Cipher.getInstance("AES");
//        cipher.init(Cipher.DECRYPT_MODE, key);
//        map = (SerializableMap<String, Integer>) sealedMap.getObject(cipher);
//        // Inspect map
//        InspectMap(map);
//    }
    // Compliant code 
    public static void main(String[] args) throws IOException, GeneralSecurityException, ClassNotFoundException {
        // Build map
        SerializableMap<String, Integer> map = buildMap();
        // Generate signing public/private key pair & sign map
        KeyPairGenerator kpg = KeyPairGenerator.getInstance("DSA");
        KeyPair kp = kpg.generateKeyPair();
        Signature sig = Signature.getInstance("SHA1withDSA");
        SignedObject signedMap = new SignedObject(map, kp.getPrivate(), sig);
        // Generate sealing key & seal map
        KeyGenerator generator;
        generator = KeyGenerator.getInstance("AES");
        generator.init(new SecureRandom());
        Key key = generator.generateKey();
        Cipher cipher = Cipher.getInstance("AES");
        cipher.init(Cipher.ENCRYPT_MODE, key);
        SealedObject sealedMap = new SealedObject(signedMap, cipher);
        // Serialize map
        ObjectOutputStream out = new ObjectOutputStream(new FileOutputStream("data"));
        out.writeObject(sealedMap);
        out.close();
        // Deserialize map
        ObjectInputStream in = new ObjectInputStream(new FileInputStream("data"));
        sealedMap = (SealedObject) in.readObject();
        in.close();
        // Unseal map
        cipher = Cipher.getInstance("AES");
        cipher.init(Cipher.DECRYPT_MODE, key);
        signedMap = (SignedObject) sealedMap.getObject(cipher);
        // Verify signature and retrieve map
        if (!signedMap.verify(kp.getPublic(), sig)) {
            throw new GeneralSecurityException("Map failed verification");
        }
        map = (SerializableMap<String, Integer>) signedMap.getObject();
        // Inspect map
        InspectMap(map);
    }
}

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Modul4.prevent_xml_external_entity_attacks;

/**
 *
 * @author REGI
 */
// Compliant code 1
import java.io.IOException;
import org.xml.sax.EntityResolver;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

class CustomResolver implements EntityResolver {

    public InputSource resolveEntity(String publicId, String systemId)
            throws SAXException, IOException {

        // Check for known good entities
        String entityPath = "file:/Users/onlinestore/good.xml";
        if (systemId.equals(entityPath)) {
            System.out.println("Resolving entity: " + publicId + " " + systemId);
            return new InputSource(entityPath);
        } else {
            // Disallow unknown entities by returning a blank path
            return new InputSource();
        }
    }
}

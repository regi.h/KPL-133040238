/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Modul4.ensure_the_array_is_filled_when_using_read_to_fill_an_array;

import java.io.DataInputStream;
import java.io.FileInputStream;
import java.io.IOException;

/**
 *
 * @author REGI
 */
public class latihan {
    // Noncompliant code 1
//    public static String readBytes(InputStream in) throws IOException {
//        byte[] data = new byte[1024];
//        if (in.read(data) == -1) {
//            throw new EOFException();
//        }
//        return new String(data, "UTF-8");
//    }
    // Noncompliant code 2
//    public static String readBytes(InputStream in) throws IOException {
//        byte[] data = new byte[1024];
//        int offset = 0;
//        if ((in.read(data, offset, data.length - offset)) != -1) {
//            throw new EOFException();
//        }
//        return new String(data, "UTF-8");
//    }

    // Compliant code 1
//    public static String readBytes(InputStream in) throws IOException {
//        int offset = 0;
//        int bytesRead = 0;
//        byte[] data = new byte[1024];
//        while ((bytesRead = in.read(data, offset, data.length - offset))
//                != -1) {
//            offset += bytesRead;
//            if (offset >= data.length) {
//                break;
//            }
//        }
//        String str = new String(data, 0, offset, "UTF-8");
//        return str;
//    }
    // Compliant code 2
    public static String readBytes(FileInputStream fis)
            throws IOException {
        byte[] data = new byte[1024];
        DataInputStream dis = new DataInputStream(fis);
        dis.readFully(data);
        String str = new String(data, "UTF-8");
        return str;
    }
}

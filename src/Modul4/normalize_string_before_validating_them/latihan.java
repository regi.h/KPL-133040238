/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Modul4.normalize_string_before_validating_them;

import java.text.Normalizer;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 *
 * @author REGI
 */
public class latihan {

    public static void main(latihan[] args) {
        // Noncompliant code
        // latihan s may be user controllable
        // \uFE64 is normalized to < and \uFE65 is normalized to > using the NFKC normalization form
//        String s = "\uFE64" + "script" + "\uFE65";
// 
//        // Validate
//        Pattern pattern = Pattern.compile("[<>]"); // Check for angle brackets
//        Matcher matcher = pattern.matcher(s);
//        if (matcher.find()) {
//          // Found black listed tag
//          throw new IllegalStateException();
//        } else {
//          // ...
//        }
// 
//        // Normalize
//        s = Normalizer.normalize(s, Form.NFKC);


        // Compliant code
        String s = "\uFE64" + "script" + "\uFE65";

        // Normalize
        s = Normalizer.normalize(s, Normalizer.Form.NFKC);

        // Validate
        Pattern pattern = Pattern.compile("[<>]");
        Matcher matcher = pattern.matcher(s);
        if (matcher.find()) {
            // Found blacklisted tag
            throw new IllegalStateException();
        } else {
            // ...
        }
    }
}

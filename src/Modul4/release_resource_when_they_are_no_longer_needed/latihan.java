/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Modul4.release_resource_when_they_are_no_longer_needed;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 *
 * @author REGI
 */
public class latihan {

    public static void sendLine(String line) {
        System.out.println("LINE : " + line);
    }

    // Noncompliant code
    public int processFile(String fileName) throws IOException, FileNotFoundException {
        FileInputStream stream = new FileInputStream(fileName);
        BufferedReader bufRead = new BufferedReader(new InputStreamReader(stream));
        String line;
        while ((line = bufRead.readLine()) != null) {
            sendLine(line);
        }
        return 1;
    }

    public static void main(String[] args) {
        // Compliant code 1
        try {
            final FileInputStream stream = new FileInputStream("fie.doc");
            try {
                final BufferedReader bufRead = new BufferedReader(new InputStreamReader(stream));
                String line;
                while ((line = bufRead.readLine()) != null) {
                    sendLine(line);
                }
            } finally {
                if (stream != null) {
                    try {
                        stream.close();
                    } catch (IOException e) {
                        // Forward to handler
                    }
                }
            }
        } catch (IOException e) {
            // Forward to handler
        }

        // Compliant code 2
        try (FileInputStream stream = new FileInputStream("file.doc");
                BufferedReader bufRead = new BufferedReader(new InputStreamReader(stream))) {
            String line;
            while ((line = bufRead.readLine()) != null) {
                sendLine(line);
            }
        } catch (IOException e) {
            // Forward to handler
        }
    }
}

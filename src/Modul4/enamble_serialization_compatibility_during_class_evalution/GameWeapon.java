/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Modul4.enamble_serialization_compatibility_during_class_evalution;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.ObjectStreamField;
import java.io.Serializable;

/**
 *
 * @author REGI
 */
// Noncompliant code
//class GameWeapon implements Serializable {
//
//    int numOfWeapons = 10;
//
//    public String toString() {
//        return String.valueOf(numOfWeapons);
//    }
//}
// Compliant code 1
//class GameWeapon implements Serializable {
//
//    private static final long serialVersionUID = 24L;
//    int numOfWeapons = 10;
//
//    public String toString() {
//        return String.valueOf(numOfWeapons);
//    }
//}
// Compliant code 2
public class GameWeapon implements Serializable {

    WeaponStore ws = new WeaponStore();
    private static final ObjectStreamField[] serialPersistentFields = {new ObjectStreamField("ws", WeaponStore.class)};

    private void readObject(ObjectInputStream ois)
            throws IOException, ClassNotFoundException {
        ObjectInputStream.GetField gf = ois.readFields();
        this.ws = (WeaponStore) gf.get("ws", ws);
    }

    private void writeObject(ObjectOutputStream oos) throws IOException {
        ObjectOutputStream.PutField pf = oos.putFields();
        pf.put("ws", ws);
        oos.writeFields();
    }

    public String toString() {
        return String.valueOf(ws);
    }
}

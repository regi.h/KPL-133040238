/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Modul4.detect_and_handle_file_related_errors;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;

/**
 *
 * @author REGI
 */
public class latihan {

    public static void main(String[] args) {
        // Noncompliant code
//        File file = new File(args[0]);
//        file.delete();

        // Compliant code
//        File file = new File("file");
//        if (!file.delete()) {
//            System.out.println("Deletion failed");
//        }

        // Compliant code 2
        Path file = new File(args[0]).toPath();
        try {
            Files.delete(file);
        } catch (IOException x) {
            System.out.println("Deletion failed");
            // Handle error
        }
    }
}

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Modul4.do_not_use_the_default_serialized_form_for_classes_with_implementation_defined_variants;

import java.io.NotSerializableException;

/***
 *
 * @author REGI
 */
public class NumberData extends Number {

    protected final Object readResolve() throws NotSerializableException {
        return INSTANCE;
    }
    private static final NumberData INSTANCE = new NumberData();

    public static NumberData getInstance() {
        return INSTANCE;
    }

    private NumberData() {
        // Perform security checks and parameter validation
    }

    protected int printData() {
        int data = 1000;
        // Print data
        return data;
    }

    @Override
    public int intValue() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public long longValue() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public float floatValue() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public double doubleValue() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}

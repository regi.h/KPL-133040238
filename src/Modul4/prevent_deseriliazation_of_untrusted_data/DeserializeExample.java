/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Modul4.prevent_deseriliazation_of_untrusted_data;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

/**
 *
 * @author REGI
 */
// Noncompliant code
//class DeserializeExample {
//
//    public static Object deserialize(byte[] buffer) throws IOException,
//            ClassNotFoundException {
//        Object ret = null;
//        try (ByteArrayInputStream bais = new ByteArrayInputStream(buffer)) {
//            try (ObjectInputStream ois = new ObjectInputStream(bais)) {
//                ret = ois.readObject();
//            }
//        }
//        return ret;
//    }
//}
// Compliant code
class DeserializeExample {

    private static Object deserialize(byte[] buffer) throws IOException,
            ClassNotFoundException {
        Object ret = null;
        Set whitelist = new HashSet<String>(Arrays.asList(new String[]{"GoodClass1", "GoodClass2"}));
        try (ByteArrayInputStream bais = new ByteArrayInputStream(buffer)) {
            try (WhitelistedObjectInputStream ois = new WhitelistedObjectInputStream(bais, whitelist)) {
                ret = ois.readObject();
            }
        }
        return ret;
    }
}

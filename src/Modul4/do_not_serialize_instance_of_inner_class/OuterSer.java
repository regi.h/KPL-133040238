/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Modul4.do_not_serialize_instance_of_inner_class;

import java.io.Serializable;

/**
 *
 * @author REGI
 */
// Noncompliant code
//public class OuterSer implements Serializable {
//
//    private int rank;
//
//    class InnerSer implements Serializable {
//
//        protected String name;
//        // ...
//    }
//}
// Compliant code 1
//public class OuterSer implements Serializable {
//
//    private int rank;
//
//    class InnerSer {
//
//        protected String name;
//        // ...
//    }
//}
// Compliant code 2
public class OuterSer implements Serializable {

    private int rank;

    static class InnerSer implements Serializable {

        protected String name;
        // ...
    }
}

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Modul4.provide_methods_to_read_and_write_little_endian_data;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

/**
 *
 * @author REGI
 */
public class latihan {
    // Compliant code 2
    // Read method

    public static int readLittleEndianInteger(InputStream ips) throws IOException {
        byte[] buffer = new byte[4];
        int check = ips.read(buffer);
        if (check != 4) {
            throw new IOException("Unexpected End of Stream");
        }
        int result = (buffer[3] << 24) | (buffer[2] << 16)
                | (buffer[1] << 8) | buffer[0];
        return result;
    }
// Write method

    public static void writeLittleEndianInteger(int i, OutputStream ops) throws IOException {
        byte[] buffer = new byte[4];
        buffer[0] = (byte) i;
        buffer[1] = (byte) (i >> 8);
        buffer[2] = (byte) (i >> 16);
        buffer[3] = (byte) (i >> 24);
        ops.write(buffer);
    }

    // Compliant code 3
    public static int reverse(int i) {
        return Integer.reverseBytes(i);
    }

    public static void main(String[] args) {
        // Noncompliant code
//        try {
//            DataInputStream dis = null;
//            try {
//                dis = new DataInputStream(new FileInputStream("data"));
//                // Little-endian data might be read as big-endian
//                int serialNumber = dis.readInt();
//            } catch (IOException x) {
//                // Handle error
//            } finally {
//                if (dis != null) {
//                    try {
//                        dis.close();
//                    } catch (IOException e) {
//                        // Handle error
//                    }
//                }
//            }
//        }
        // Compliant code 1
//        try {
//            DataInputStream dis = null;
//            try {
//                dis = new DataInputStream(new FileInputStream("data"));
//                byte[] buffer = new byte[4];
//                int bytesRead = dis.read(buffer); // Bytes are read into buffer
//                if (bytesRead != 4) {
//                    throw new IOException("Unexpected End of Stream");
//                }
//                int serialNumber = ByteBuffer.wrap(buffer).order(ByteOrder.LITTLE_ENDIAN).getInt();
//            } finally {
//                if (dis != null) {
//                    try {
//                        dis.close();
//                    } catch (IOException x) {
//                        // Handle error
//                    }
//                }
//            }
//        } catch (IOException x) {
//            // Handle error
//        }
    }
}

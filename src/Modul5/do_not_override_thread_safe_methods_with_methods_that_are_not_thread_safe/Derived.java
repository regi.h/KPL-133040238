/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Modul5.do_not_override_thread_safe_methods_with_methods_that_are_not_thread_safe;

/**
 *
 * @author REGI
 */
public class Derived extends Base {

    // Noncompliant code
//    @Override
//    public void doSomething() {
//        System.out.println("Derived");
//    }
    // Compliant code 1
//    @Override
//    public synchronized void doSomething() {
//        System.out.println("Derived");
//    }
    // Compliant code 2
    private final Object lock = new Object();

    @Override
    public void doSomething() {
        synchronized (lock) {
// ...
        }
    }
}

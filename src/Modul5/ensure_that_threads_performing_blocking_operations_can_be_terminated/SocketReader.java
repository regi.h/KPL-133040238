/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Modul5.ensure_that_threads_performing_blocking_operations_can_be_terminated;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.SocketChannel;

/**
 *
 * @author REGI
 */
// Noncompliant code 1
// Thread-safe class
//public final class SocketReader implements Runnable {
//
//    private final Socket socket;
//    private final BufferedReader in;
//    private volatile boolean done = false;
//    private final Object lock = new Object();
//
//    public SocketReader(String host, int port) throws IOException {
//        this.socket = new Socket(host, port);
//        this.in = new BufferedReader(
//                new InputStreamReader(this.socket.getInputStream())
//        );
//    }
//// Only one thread can use the socket at a particular time
//
//    @Override
//    public void run() {
//        try {
//            synchronized (lock) {
//                readData();
//            }
//        } catch (IOException ie) {
//// Forward to handler
//        }
//    }
//
//    public void readData() throws IOException {
//        String string;
//        while (!done && (string = in.readLine()) != null) {
//// Blocks until end of stream (null)
//        }
//    }
//
//    public void shutdown() {
//        done = true;
//    }
//
//    public static void main(String[] args)
//            throws IOException, InterruptedException {
//        SocketReader reader = new SocketReader("somehost", 25);
//        Thread thread = new Thread(reader);
//        thread.start();
//        Thread.sleep(1000);
//        reader.shutdown(); // Shut down the thread
//    }
//}
// Noncompliant code 2
// Thread-safe class
//public final class SocketReader implements Runnable {
//
//    private final Socket socket;
//    private final BufferedReader in;
//    private volatile boolean done = false;
//    private final Object lock = new Object();
//
//    public SocketReader(String host, int port) throws IOException {
//        this.socket = new Socket(host, port);
//        this.in = new BufferedReader(
//                new InputStreamReader(this.socket.getInputStream())
//        );
//    }
//// Only one thread can use the socket at a particular time
//
//    @Override
//    public void run() {
//        try {
//            synchronized (lock) {
//                readData();
//            }
//        } catch (IOException ie) {
//// Forward to handler
//        }
//    }
//
//    public void readData() throws IOException {
//        String string;
//        while (!Thread.interrupted() && (string = in.readLine()) != null) {
//// Blocks until end of stream (null)
//        }
//    }
//
//    public static void main(String[] args)
//            throws IOException, InterruptedException {
//        SocketReader reader = new SocketReader("somehost", 25);
//        Thread thread = new Thread(reader);
//        thread.start();
//        Thread.sleep(1000);
//        thread.interrupt(); // Interrupt the thread
//    }
//}
// Compliant code 1
//public final class SocketReader implements Runnable {
//
//    private final Socket socket;
//    private final BufferedReader in;
//    private volatile boolean done = false;
//    private final Object lock = new Object();
//
//    public SocketReader(String host, int port) throws IOException {
//        this.socket = new Socket(host, port);
//        this.in = new BufferedReader(
//                new InputStreamReader(this.socket.getInputStream())
//        );
//    }
//// Only one thread can use the socket at a particular time
//
//    @Override
//    public void run() {
//        try {
//            synchronized (lock) {
//                readData();
//            }
//        } catch (IOException ie) {
//// Forward to handler
//        }
//    }
//
//    public void readData() throws IOException {
//        String string;
//        try {
//            while ((string = in.readLine()) != null) {
//// Blocks until end of stream (null)
//            }
//        } finally {
//            shutdown();
//        }
//    }
//
//    public void shutdown() throws IOException {
//        socket.close();
//    }
//
//    public static void main(String[] args)
//            throws IOException, InterruptedException {
//        SocketReader reader = new SocketReader("somehost", 25);
//        Thread thread = new Thread(reader);
//        thread.start();
//        Thread.sleep(1000);
//        reader.shutdown();
//    }
//}
// Compliant code 2
public final class SocketReader implements Runnable {

    private final SocketChannel sc;
    private final Object lock = new Object();

    public SocketReader(String host, int port) throws IOException {
        sc = SocketChannel.open(new InetSocketAddress(host, port));
    }

    @Override
    public void run() {
        ByteBuffer buf = ByteBuffer.allocate(1024);
        try {
            synchronized (lock) {
                while (!Thread.interrupted()) {
                    sc.read(buf);
// ...
                }
            }
        } catch (IOException ie) {
// Forward to handler
        }
    }

    public static void main(String[] args)
            throws IOException, InterruptedException {
        SocketReader reader = new SocketReader("somehost", 25);
        Thread thread = new Thread(reader);
        thread.start();
        Thread.sleep(1000);
        thread.interrupt();
    }
}

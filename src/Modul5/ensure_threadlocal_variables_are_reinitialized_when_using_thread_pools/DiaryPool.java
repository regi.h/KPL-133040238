/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Modul5.ensure_threadlocal_variables_are_reinitialized_when_using_thread_pools;

import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.Executor;
import java.util.concurrent.TimeUnit;

/**
 *
 * @author REGI
 */
// Noncompliant code 1
public final class DiaryPool {

    // Noncompliant code 1
    final int numOfThreads = 2; // Maximum number of threads allowed in pool
    // Noncompliant code 2
    final int numOfthreads = 3;
    final Executor exec;
    final Diary diary;

    // Noncompliant code 1
//    DiaryPool() {
//        exec = (Executor) Executors.newFixedThreadPool(numOfThreads);
//        diary = new Diary();
//    }
    // Compliant code 2
    DiaryPool() {
        exec = new CustomThreadPoolExecutor(NumOfthreads, NumOfthreads, 10, TimeUnit.SECONDS, new ArrayBlockingQueue<Runnable>(10));
        diary = new Diary();
    }

    // Noncompliant code 1
//    public void doSomething1() {
//        exec.execute(new Runnable() {
//            @Override
//            public void run() {
//                diary.setDay(Day.FRIDAY);
//                diary.threadSpecificTask();
//            }
//        });
//    }
    // Compliant code 1
    public void doSomething1() {
        exec.execute(new Runnable() {
            @Override
            public void run() {
                try {
                    Diary.setDay(Diary.Day.FRIDAY);
                    diary.threadSpecificTask();
                } finally {
                    Diary.removeDay(); // Diary.setDay(Day.MONDAY)
                    // can also be used
                }
            }
        });
    }

    public void doSomething2() {
        exec.execute(new Runnable() {
            @Override
            public void run() {
                diary.threadSpecificTask();
            }
        });
    }

    public static void main(String[] args) {
        DiaryPool dp = new DiaryPool();
        dp.doSomething1(); // Thread 1, requires current day as Friday
        dp.doSomething2(); // Thread 2, requires current day as Monday
        dp.doSomething2(); // Thread 3, requires current day as Monday
    }
}

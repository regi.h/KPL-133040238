/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Modul5.ensure_threadlocal_variables_are_reinitialized_when_using_thread_pools;

/**
 *
 * @author REGI
 */
public final class Diary {

    static void removeDay() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public enum Day {

        MONDAY, TUESDAY, WEDNESDAY, THURSDAY, FRIDAY, SATURDAY, SUNDAY;
    }
    private static final ThreadLocal<Diary.Day> days = new ThreadLocal<Diary.Day>() {
        // Initialize to Monday
        protected Diary.Day initialValue() {
            return Diary.Day.MONDAY;
        }
    };

    private static Diary.Day currentDay() {
        return days.get();
    }

    public static void setDay(Diary.Day newDay) {
        days.set(newDay);
    }
// Performs some thread-specific task

    public void threadSpecificTask() {
// Do task ...
    }
}

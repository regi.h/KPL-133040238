/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Modul5.ensure_threadlocal_variables_are_reinitialized_when_using_thread_pools;

import Modul5.ensure_threadlocal_variables_are_reinitialized_when_using_thread_pools.Diary.Day;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

/**
 *
 * @author REGI
 */
// Compliant code 2
class CustomThreadPoolExecutor extends ThreadPoolExecutor {

    public CustomThreadPoolExecutor(int corePoolSize,
            int maximumPoolSize, long keepAliveTime,
            TimeUnit unit, BlockingQueue<Runnable> workQueue) {
        super(corePoolSize, maximumPoolSize, keepAliveTime,
                unit, workQueue);
    }

    @Override
    public void beforeExecute(Thread t, Runnable r) {
        if (t == null || r == null) {
            throw new NullPointerException();
        }
        Diary.setDay(Day.MONDAY);
        super.beforeExecute(t, r);
    }
}

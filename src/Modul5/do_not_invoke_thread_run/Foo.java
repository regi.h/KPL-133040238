/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Modul5.do_not_invoke_thread_run;

/**
 *
 * @author REGI
 */
// noncompliant code
//public final class Foo implements Runnable {
//
//    @Override
//    public void run() {
//// ...
//    }
//
//    public static void main(String[] args) {
//        Foo foo = new Foo();
//        new Thread(foo).run();
//    }
//}
// Compliant code 
public final class Foo implements Runnable {

    @Override
    public void run() {
// ...
    }

    public static void main(String[] args) {
        Foo foo = new Foo();
        new Thread(foo).start();
    }

    // Exception
    public void sampleRunTest() {
        Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {
// ...
            }
        });
        ((Runnable) thread).run(); // THI00-J-EX0: Does not start a new thread
    }
}

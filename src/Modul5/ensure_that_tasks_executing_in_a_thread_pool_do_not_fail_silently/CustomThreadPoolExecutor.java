/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Modul5.ensure_that_tasks_executing_in_a_thread_pool_do_not_fail_silently;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

/**
 *
 * @author REGI
 */
// Compliant code 1
class CustomThreadPoolExecutor extends ThreadPoolExecutor {
// ... Constructor ...

    public CustomThreadPoolExecutor(
            int corePoolSize, int maximumPoolSize, long keepAliveTime, TimeUnit unit, BlockingQueue<Runnable> workQueue) {
        super(corePoolSize, maximumPoolSize, keepAliveTime, unit, workQueue);
    }

    @Override
    public void afterExecute(Runnable r, Throwable t) {
        super.afterExecute(r, t);
        if (t != null) {
            // Exception occurred, forward to handler
        }
        // ... Perform task-specific cleanup actions
    }

    @Override
    public void terminated() {
        super.terminated();
        // ... Perform final clean-up actions
    }
}

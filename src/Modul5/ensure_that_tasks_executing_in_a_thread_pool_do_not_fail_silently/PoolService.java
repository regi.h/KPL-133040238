/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Modul5.ensure_that_tasks_executing_in_a_thread_pool_do_not_fail_silently;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

/**
 *
 * @author REGI
 */
// Noncompliant code
//final class PoolService {
//
//    private final ExecutorService pool = Executors.newFixedThreadPool(10);
//
//    public void doSomething() {
//        pool.execute(new Task() {
//
//            @Override
//            protected Object call() throws Exception {
//                throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
//            }
//        });
//    }
//}
// Compliantt code 1
//final class PoolService {
//    // The values have been hard-coded for brevity
//
//    ExecutorService pool = new CustomThreadPoolExecutor(10, 10, 10, TimeUnit.SECONDS, new ArrayBlockingQueue<Runnable>(10));
//    // ...
//}
// Compliant code 2
//final class PoolService {
//
//    private static final ThreadFactory factory = new ExceptionThreadFactory(new MyExceptionHandler());
//    private static final ExecutorService pool
//            = Executors.newFixedThreadPool(10, factory);
//
//    public void doSomething() {
//        pool.execute(new Task()); // Task is a runnable class
//    }
//
//    public static class ExceptionThreadFactory implements ThreadFactory {
//
//        private static final ThreadFactory defaultFactory
//                = Executors.defaultThreadFactory();
//        private final Thread.UncaughtExceptionHandler handler;
//
//        public ExceptionThreadFactory(
//                Thread.UncaughtExceptionHandler handler) {
//            this.handler = handler;
//        }
//
//        @Override
//        public Thread newThread(Runnable run) {
//            Thread thread = defaultFactory.newThread(run);
//            thread.setUncaughtExceptionHandler(handler);
//            return thread;
//        }
//    }
//
//    public static class MyExceptionHandler extends ExceptionReporter implements Thread.UncaughtExceptionHandler {
//// ...
//
//        @Override
//        public void uncaughtException(Thread thread, Throwable t) {
//// Recovery or logging code
//        }
//    }
//}
// Compliant code 3
final class PoolService {

    private final ExecutorService pool = Executors.newFixedThreadPool(10);

    public void doSomething() {
        Future<?> future = pool.submit(new Task());
// ...
        try {
            future.get();
        } catch (InterruptedException e) {
            Thread.currentThread().interrupt(); // Reset interrupted status
        } catch (ExecutionException e) {
            Throwable exception = e.getCause();
// Forward to exception reporter
        }
    }
}

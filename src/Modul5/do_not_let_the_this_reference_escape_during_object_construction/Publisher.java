/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Modul5.do_not_let_the_this_reference_escape_during_object_construction;

/**
 *
 * @author REGI
 */
// Noncompliant code 1 Publish before initialization
//final class Publisher {
//
//    public static volatile Publisher published;
//    int num;
//
//    Publisher(int number) {
//        published = this;
//// Initialization
//        this.num = number;
//// ...
//    }
//}
// Noncompliant code 2 Nonvolatile Public static field
//final class Publisher {
//
//    public static Publisher published;
//    int num;
//
//    Publisher(int number) {
//// Initialization
//        this.num = number;
//// ...
//        published = this;
//    }
//}
// Compliant code 1 Volatile field and publish after initialization
//final class Publisher {
//
//    static volatile Publisher published;
//    int num;
//
//    Publisher(int number) {
//// Initialization
//        this.num = number;
//// ...
//        published = this;
//    }
//}
// Compliant code 2 Public static factory method
final class Publisher {

    final int num;

    private Publisher(int number) {
// Initialization
        this.num = number;
    }

    public static Publisher newInstance(int number) {
        Publisher published = new Publisher(number);
        return published;
    }
}

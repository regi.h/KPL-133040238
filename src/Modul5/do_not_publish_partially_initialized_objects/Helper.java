/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Modul5.do_not_publish_partially_initialized_objects;

/**
 *
 * @author REGI
 */
//public class Helper {
//
//    private int n;
//
//    public Helper(int n) {
//        this.n = n;
//    }
//// ...
//}
// Compliant code 5 Imutable Object - Final Fields, Volatile Resource
//public final class Helper {
//
//    private final int n;
//
//    public Helper(int n) {
//        this.n = n;
//    }
//// ...
//}
// Compliant code 6 Thread-Safe Object, Volatile Reference
// Mutable but thread-safe Helper
public class Helper {

    private volatile int n;
    private final Object lock = new Object();

    public Helper(int n) {
        this.n = n;
    }

    public void setN(int value) {
        synchronized (lock) {
            n = value;
        }
    }
}

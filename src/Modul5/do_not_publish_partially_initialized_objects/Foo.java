/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Modul5.do_not_publish_partially_initialized_objects;

/**
 *
 * @author REGI
 */
// Noncompliant code 
//class Foo {
//
//    private Helper helper;
//
//    public Helper getHelper() {
//        return helper;
//    }
//
//    public void initialize() {
//        helper = new Helper(42);
//    }
//}
// Compliant code 1 Sychronized 
//class Foo {
//
//    private Helper helper;
//
//    public synchronized Helper getHelper() {
//        return helper;
//    }
//
//    public synchronized void initialize() {
//        helper = new Helper(42);
//    }
//}
// Compliant code 2 Final field
//class Foo {
//
//    private final Helper helper;
//
//    public Helper getHelper() {
//        return helper;
//    }
//
//    public Foo() {
//        // Point 1
//        helper = new Helper(42);
//        // Point 2
//    }
//}
// Compliant code 3 final field and thread-safe composition
//class Foo {
//
//    private final Vector<Helper> helper;
//
//    public Foo() {
//        helper = new Vector<Helper>();
//    }
//
//    public Helper getHelper() {
//        if (helper.isEmpty()) {
//            initialize();
//        }
//        return helper.elementAt(0);
//    }
//
//    public synchronized void initialize() {
//        if (helper.isEmpty()) {
//            helper.add(new Helper(42));
//        }
//    }
//}
// Compliant code 4 Static Initialization
//final class Foo {
//
//    private static final Helper helper = new Helper(42);
//
//    public static Helper getHelper() {
//        return helper;
//    }
//}
// Compliant code 5 Imutable Object - Final Fields, Volatile Resource
//class Foo {
//
//    private volatile Helper helper;
//
//    public Helper getHelper() {
//        return helper;
//    }
//
//    public void initialize() {
//        helper = new Helper(42);
//    }
//}
// Compliant code 6 Thread-Safe Object, Volatile Reference
class Foo {

    private volatile Helper helper;

    public Helper getHelper() {
        return helper;
    }

    public void initialize() {
        helper = new Helper(42);
    }
}

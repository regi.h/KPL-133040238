/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Modul5.notify_all_waiting_threads_rather_than_a_single_thread;

/**
 *
 * @author REGI
 */
// Noncompliant code
//public final class ProcessStep implements Runnable {
//
//    private static final Object lock = new Object();
//    private static int time = 0;
//    private final int step; // Do Perform operations when field time
//    // reaches this value
//
//    public ProcessStep(int step) {
//        this.step = step;
//    }
//
//    @Override
//    public void run() {
//        try {
//            synchronized (lock) {
//                while (time != step) {
//                    lock.wait();
//                }
//                // Perform operations
//                time++;
//                lock.notify();
//            }
//        } catch (InterruptedException ie) {
//            Thread.currentThread().interrupt(); // Reset interrupted status
//        }
//    }
//
//    public static void main(String[] args) {
//        for (int i = 4; i >= 0; i--) {
//            new Thread(new ProcessStep(i)).start();
//        }
//    }
//}
// Compliant code
public final class ProcessStep implements Runnable {

    private static final Object lock = new Object();
    private static int time = 0;
    private final int step; // Perform operations when field time
    // reaches this value

    public ProcessStep(int step) {
        this.step = step;
    }

    @Override
    public void run() {
        try {
            synchronized (lock) {
                while (time != step) {
                    lock.wait();
                }
                // Perform operations
                time++;
                lock.notifyAll(); // Use notifyAll() instead of notify()
            }
        } catch (InterruptedException ie) {
            Thread.currentThread().interrupt(); // Reset interrupted status
        }
    }
}

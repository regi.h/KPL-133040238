/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Modul5.use_thread_pools_to_enable_graceful_degradation_of_service_during_traffic_bursts;

import java.io.IOException;
import java.net.ServerSocket;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

/**
 *
 * @author REGI
 */
// Noncompliant code
//final class RequestHandler {
//
//    private final Helper helper = new Helper();
//    private final ServerSocket server;
//
//    private RequestHandler(int port) throws IOException {
//        server = new ServerSocket(port);
//    }
//
//    public static RequestHandler newInstance() throws IOException {
//        return new RequestHandler(0); // Selects next available port
//    }
//
//    public void handleRequest() {
//        new Thread(new Runnable() {
//            public void run() {
//                try {
//                    helper.handle(server.accept());
//                } catch (IOException e) {
//                    // Forward to handler
//                }
//            }
//        }).start();
//    }
//}
// COmpliant code
final class RequestHandler {

    private final Helper helper = new Helper();
    private final ServerSocket server;
    private final ExecutorService exec;

    private RequestHandler(int port, int poolSize) throws IOException {
        server = new ServerSocket(port);
        exec = Executors.newFixedThreadPool(poolSize);
    }

    public static RequestHandler newInstance(int poolSize)
            throws IOException {
        return new RequestHandler(0, poolSize);
    }

    public void handleRequest() {
        Future<?> future = exec.submit(new Runnable() {
            @Override
            public void run() {
                try {
                    helper.handle(server.accept());
                } catch (IOException e) {
                    // Forward to handler
                }
            }
        });
    }
// ... Other methods such as shutting down the thread pool
// and task cancellation ...
}

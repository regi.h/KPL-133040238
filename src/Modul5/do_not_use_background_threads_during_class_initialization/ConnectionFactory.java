/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Modul5.do_not_use_background_threads_during_class_initialization;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 *
 * @author REGI
 */
// Noncompliant code
//public final class ConnectionFactory {
//
//    private static Connection dbConnection;
//    // Other fields ...
//
//    static {
//        Thread dbInitializerThread = new Thread(new Runnable() {
//            @Override
//            public void run() {
//                // Initialize the database connection
//                try {
//                    dbConnection = DriverManager.getConnection("connection string");
//                } catch (SQLException e) {
//                    dbConnection = null;
//                }
//            }
//        });
//        // Other initialization, for example, start other threads
//        dbInitializerThread.start();
//        try {
//            dbInitializerThread.join();
//        } catch (InterruptedException ie) {
//            throw new AssertionError(ie);
//        }
//    }
//
//    public static Connection getConnection() {
//        if (dbConnection == null) {
//            throw new IllegalStateException("Error initializing connection");
//        }
//        return dbConnection;
//    }
//
//    public static void main(String[] args) {
//        // ...
//        Connection connection = getConnection();
//    }
//}
// Compliant code 1
//public final class ConnectionFactory {
//
//    private static Connection dbConnection;
//    // Other fields ...
//
//    static {
//        Thread dbInitializerThread = new Thread(new Runnable() {
//            @Override
//            public void run() {
//                // Initialize the database connection
//                try {
//                    dbConnection = DriverManager.getConnection("connection string");
//                } catch (SQLException e) {
//                    dbConnection = null;
//                }
//            }
//        });
//        // Other initialization, for example, start other threads
//        dbInitializerThread.start();
//        try {
//            dbInitializerThread.join();
//        } catch (InterruptedException ie) {
//            throw new AssertionError(ie);
//        }
//    }
//
//    public static Connection getConnection() {
//        if (dbConnection == null) {
//            throw new IllegalStateException("Error initializing connection");
//        }
//        return dbConnection;
//    }
//
//    public static void main(String[] args) {
//// ...
//        Connection connection = getConnection();
//    }
//}
// compliant code 2
public final class ConnectionFactory {

    private static final ThreadLocal<Connection> connectionHolder = new ThreadLocal<Connection>() {
        @Override
        public Connection initialValue() {
            try {
                Connection dbConnection = DriverManager.getConnection("connection string");
                return dbConnection;
            } catch (SQLException e) {
                return null;
            }
        }
    };
// Other fields ...

    // Other initialization (do not start any threads)}
    public static Connection getConnection() {
        Connection connection = connectionHolder.get();
        if (connection == null) {
            throw new IllegalStateException("Error initializing connection");
        }
        return connection;
    }

    public static void main(String[] args) {
// ...
        Connection connection = getConnection();
    }
}

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Modul5.ensure_that_tasks_submitted_to_a_thread_pool_are_interruptable;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.SocketChannel;

/**
 *
 * @author REGI
 */
// Noncompliant code
//public final class SocketReader implements Runnable { // Thread-safe
//
//        private final Socket socket;
//        private final BufferedReader in;
//        private final Object lock = new Object();
//
//        public SocketReader(String host, int port) throws IOException {
//            this.socket = new Socket(host, port);
//            this.in = new BufferedReader(new InputStreamReader(this.socket.getInputStream())
//            );
//        }
//// Only one thread can use the socket at a particular time
//
//        @Override
//        public void run() {
//            try {
//                synchronized (lock) {
//                    readData();
//                }
//            } catch (IOException ie) {
//// Forward to handler
//            }
//        }
//
//        public void readData() throws IOException {
//            String string;
//            try {
//                while ((string = in.readLine()) != null) {
//// Blocks until end of stream (null)
//                }
//            } finally {
//                shutdown();
//            }
//        }
//
//        public void shutdown() throws IOException {
//            socket.close();
//        }
//    
//}
// Compliant code
public final class SocketReader implements Runnable {

    private final SocketChannel sc;
    private final Object lock = new Object();

    public SocketReader(String host, int port) throws IOException {
        sc = SocketChannel.open(new InetSocketAddress(host, port));
    }

    @Override
    public void run() {
        ByteBuffer buf = ByteBuffer.allocate(1024);
        try {
            synchronized (lock) {
                while (!Thread.interrupted()) {
                    sc.read(buf);
// ...
                }
            }
        } catch (IOException ie) {
// Forward to handler
        }
    }
}

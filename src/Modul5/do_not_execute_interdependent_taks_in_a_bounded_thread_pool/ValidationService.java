/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Modul5.do_not_execute_interdependent_taks_in_a_bounded_thread_pool;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 *
 * @author REGI
 */
public class ValidationService {

    private final ExecutorService pool;

    public ValidationService(int poolSize) {
        pool = Executors.newFixedThreadPool(poolSize);
    }

    public void shutdown() {
        pool.shutdown();
    }

    // Noncompliant code
//    public StringBuilder fieldAggregator(String... inputs) throws InterruptedException, ExecutionException {
//        StringBuilder sb = new StringBuilder();
//// Stores the results
//        Future<String>[] results = new Future[inputs.length];
//// Submits the tasks to thread pool
//        for (int i = 0; i < inputs.length; i++) {
//            results[i] = pool.submit(new ValidateInput<String>(inputs[i], pool));
//        }
//        for (int i = 0; i < inputs.length; i++) { // Aggregates the results
//            sb.append(results[i].get());
//        }
//        return sb;
//    }
    // Compliant code
    public StringBuilder fieldAggregator(String... inputs)
            throws InterruptedException, ExecutionException {
        // ...
        for (int i = 0; i < inputs.length; i++) {
            // Don't pass-in thread pool
            results[i] = pool.submit(new ValidateInput<String>(inputs[i]));
        }
        // ...
        return null;
    }
}

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Modul5.do_not_execute_interdependent_taks_in_a_bounded_thread_pool;

/**
 *
 * @author REGI
 */
//public final class SanitizeInput<V> implements Callable<V> {
//
//    private final V input;
//
//    SanitizeInput(V input) {
//        this.input = input;
//    }
//
//    @Override
//    public V call() throws Exception {
//// Sanitize input and return
//        return (V) input;
//    }
//}
// Compliant code
public final class SanitizeInput<V> { // No longer a Callable task

    public SanitizeInput() {
    }

    public V sanitize(V input) {
        // Sanitize input and return
        return input;
    }
}

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Modul5.do_not_execute_interdependent_taks_in_a_bounded_thread_pool;

import java.util.concurrent.Callable;

/**
 *
 * @author REGI
 */
//public final class ValidateInput<V> implements Callable<V> {
//
//    private final V input;
//    private final ExecutorService pool;
//
//    ValidateInput(V input, ExecutorService pool) {
//        this.input = input;
//        this.pool = pool;
//    }
//
//    @Override
//    public V call() throws Exception {
//// If validation fails, throw an exception here
//// Subtask
//        Future<V> future = pool.submit(new SanitizeInput<V>(input));
//        return (V) future.get();
//    }
//}
// Compliant code
// Does not use same thread pool
public final class ValidateInput<V> implements Callable<V> {

    private final V input;

    ValidateInput(V input) {
        this.input = input;
    }

    @Override
    public V call() throws Exception {
        // If validation fails, throw an exception here
        return (V) new SanitizeInput().sanitize(input);
    }
}

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Modul2.provide_mutable_classes_with_copy_functionallity_to_safety_allow_passing_instances_to_untrusted_code;

import java.util.Date;

/**
 *
 * @author REGI
 */
public class UnmodifiableDateView {

    private Date date;

    public UnmodifiableDateView(Date date) {
        this.date = date;
    }

    public void setTime(long date) {
        throw new UnsupportedOperationException();
    }
    // Override all other mutator methods to throw UnsupportedOperationException
}

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Modul2.provide_mutable_classes_with_copy_functionallity_to_safety_allow_passing_instances_to_untrusted_code;

import java.util.Date;

/**
 *
 * @author REGI
 */
public class MutableClass {
    // Noncompliant code
//    private Date date;
//    public MutableClass(Date d) {
//        this.date = d;
//    }
//    
//    public void setDate(Date d) {
//        this.date = d;
//    }
//    
//    public Date getDate() {
//        return date;
//    }

    // Compliant code solution 1 Copy Constructor
//    private final Date date;
//    public MutableClass(MutableClass mc) {
//        this.date = new Date(mc.date.getTime());
//    }
//    
//    public MutableClass(Date d) {
//        this.date = new Date(d.getTime()); // Make defensive copy
//    }
//    
//    public Date getDate() {
//        return (Date) date.clone(); // Copy and return
//    }
    // Compliant code solution 2 Public static Factory Method
//    private final Date date;
//    private MutableClass(Date d) { // Noninstantiable and nonsubclassable
//        this.date = new Date(d.getTime()); // Make defensive copy
//    }
//    
//    public Date getDate() {
//        return (Date) date.clone(); // Copy and return
//    }
//    
//    public static MutableClass getInstance(MutableClass mc) {
//        return new MutableClass(mc.getDate());
//    }
    // Compliant code solution 3 clone
//    private Date date;
//    public MutableClass(Date d) {
//        this.date = new Date(d.getTime());
//    }
//    
//    public Date getDate() {
//        return (Date) date.clone();
//    }
//    
//    public void setDate(Date d) {
//        this.date = (Date) d.clone();
//    }
//    
//    public Object clone() throws CloneNotSupportedException {
//        final MutableClass cloned = (MutableClass) super.clone();
//        cloned.date = (Date) date.clone(); // Manually copy mutable Dat object
//        return cloned;
//    }
    // Compliant code solution 4 clone() with final members
//    private final Date date; // final field
//    public MutableClass(Date d) {
//        this.date = new Date(d.getTime()); // Copy in
//    }
//    
//    public Date getDate() {
//        return (Date) date.clone(); // Copy and return
//    }
//    
//    public Object clone() {
//        Date d = (Date) date.clone();
//        MutableClass cloned = new MutableClass(d);
//        return cloned;
//    }
    // Compliant code solution 5 Unmodifiable Date Wrapper
    private Date date;

    public MutableClass(Date d) {
        this.date = d;
    }

    public void setDate(Date d) {
        this.date = (Date) d.clone();
    }

    public UnmodifiableDateView getDate() {
        return new UnmodifiableDateView(date);
    }
}

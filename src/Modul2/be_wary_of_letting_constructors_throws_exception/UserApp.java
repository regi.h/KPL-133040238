/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Modul2.be_wary_of_letting_constructors_throws_exception;

/**
 *
 * @author REGI
 */
public class UserApp {

    public static void main(String[] args) {
        BankOperations bo;
        try {
            bo = new BankOperations();
        } catch (SecurityException ex) {
            bo = null;
        }
        Storage.store(bo);
        System.out.println("Proceed with normal logic");
    }
}

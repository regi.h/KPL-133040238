/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Modul2.be_wary_of_letting_constructors_throws_exception;

/**
 *
 * @author REGI
 */
public class Storage {

    private static BankOperations bop;

    public static void store(BankOperations bo) {
        // Store only if it is initialized
        if (bop == null) {
            if (bo == null) {
                System.out.println("Invalid object!");
                System.exit(1);
            }
            bop = bo;
        }
    }
}

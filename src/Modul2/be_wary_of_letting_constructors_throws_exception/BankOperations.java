/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Modul2.be_wary_of_letting_constructors_throws_exception;

/**
 *
 * @author REGI
 */
public class BankOperations {

    private volatile boolean initialized = false;
    // Noncompliant code
//    public BankOperations() {
//        if (!performSSNVerification()) {
//            throw new SecurityException("Access Denied!");
//        }
//    }

    // Compliant code 3
//    public BankOperations() {
//        this(performSSNVerification());
//    }
//    
//    private BankOperations(boolean secure) {
//        // secure is always true
//        // Constructor without any security checks
//    }
    // Compliant code 4
    public BankOperations() {
        if (!performSSNVerification()) {
            return; // object construction failed
        }
        this.initialized = true; // Object construction successful
    }

    // Compliant code 3
//    private static boolean performSSNVerification() {
//        // Returns true if data entered is valid, else throws a SecurityException
//        // Assume that the attacker just enters invalid SSN, so this method always throws the exception
//        throw new SecurityException("Invalid SSN!");
//    }
    // ...remainder of BankOperations class definition
    // Compliant code 4
    private boolean performSSNVerification() {
        return false;
    }

    public void greet() {
        if (!this.initialized) {
            throw new SecurityException("Invalid SSN!");
        }
        System.out.println(
                "Welcome user! You may now use all the features.");
    }

    // Noncompliant code
//    private boolean performSSNVerification() {
//        return false; // Returns true if data entered is valid, else false
//        // Assume that the attacker always enters an invalid SSN
//    }
    public void greet() {
        System.out.println("Welcome user! You may now use all the"
                + "features.");
    }

    // Compliant code 2
    public final void finalize() {
        // Do nothing
    }
}

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Modul2.preserve_dependencies_in_subclass_when_changing_superclass;

import java.util.Calendar;

/**
 *
 * @author REGI
 */
public class ForwardingCalendar {

    private final CalendarImplementation c;

    public ForwardingCalendar(CalendarImplementation c) {
        this.c = c;
    }

    CalendarImplementation getCalendarImplementation() {
        return c;
    }

    public boolean after(Object when) {
        return c.after(when);
    }

    public int compareTo(Calendar anotherCalendar) {
        // CalendarImplementation.compareTo() will be called
        return c.compareTo(anotherCalendar);
    }
}

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Modul2.preserve_dependencies_in_subclass_when_changing_superclass;

import java.util.Calendar;
import java.util.Date;

/**
 *
 * @author REGI
 */
public class CalendarSubclass extends Calendar {

    @Override
    public boolean after(Object when) {// Correctly calls Calendar.compareTo()
        if (when instanceof Calendar
                && super.compareTo((Calendar) when) == 0) {
            return true;
        }
        return super.after(when);
    }

    @Override
    public int compareTo(Calendar anotherCalendar) {
        return compareDays(this.getFirstDayOfWeek(),
                anotherCalendar.getFirstDayOfWeek());
    }

    private int compareDays(int currentFirstDayOfWeek, int anotherFirstDayOfWeek) {
        return (currentFirstDayOfWeek > anotherFirstDayOfWeek) ? 1 : (currentFirstDayOfWeek == anotherFirstDayOfWeek) ? 0 : -1;
    }

    public static void main(String[] args) {
        CalendarSubclass cs1 = new CalendarSubclass();
        cs1.setTime(new Date());
        // Date of last Sunday (before now)
        cs1.set(Calendar.DAY_OF_WEEK, Calendar.SUNDAY);
        // Wed Dec 31 19:00:00 EST 1969
        CalendarSubclass cs2 = new CalendarSubclass();
        // Expected to print true
        System.out.println(cs1.after(cs2));
    }
    // Implementation of other Calendar abstract methods

    @Override
    protected void computeTime() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    protected void computeFields() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void add(int field, int amount) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void roll(int field, boolean up) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public int getMinimum(int field) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public int getMaximum(int field) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public int getGreatestMinimum(int field) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public int getLeastMaximum(int field) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}

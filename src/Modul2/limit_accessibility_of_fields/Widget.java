/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Modul2.limit_accessibility_of_fields;

import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;

/**
 *
 * @author REGI
 */
public class Widget {
    // Bagian 1
    // Noncompliant code
//    public int total; // Number of elements

    void add() {
        if (total < Integer.MAX_VALUE) {
            total++;
            // ...
        } else {
            throw new ArithmeticException("Overflow");
        }
    }

    void remove() {
        if (total > 0) {
            total--;
            // ...
        } else {
            throw new ArithmeticException("Overflow");
        }
    }
    // Compliant code solution
    private int total;

    public int getTotal() {
        return total;
    }
    // Bagian 2
    // Noncompliant code
//    public static final HashMap<Integer, String> hm = new HashMap<Integer, String>();
    // Compliant code solution
    private static final HashMap<Integer, String> hm = new HashMap<Integer, String>();

    public static String getElement(int key) {
        return hm.get(key);
    }
    // Bagian 3
    // Noncompliant code
//    public static final String[] items = {"baju", "celana", "sepatu"};
    // Compliant code solution 1
//    private static final String[] items = {"baju", "celana", "sepatu"};
//    public static final String getItem(int index) {
//        return items[index];
//    }
//    public static final int getItemCount() {
//        return items.length;
//    }
    // Compliant code solution 2
//    private static final String[] items = {"baju", "celana", "sepatu"};
//    public static String[] getItems(){
//        return items.clone();
//    }
    // Compliant code solution 3
    private static final String[] items = {"baju", "celana", "sepatu"};
    public static final List<String> itemsList = Collections.unmodifiableList(Arrays.asList(items));
}

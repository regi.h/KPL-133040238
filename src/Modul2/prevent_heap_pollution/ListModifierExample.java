/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Modul2.prevent_heap_pollution;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 *
 * @author REGI
 */
public class ListModifierExample {
    // Noncompliant code 1
//    public static void modify(List<String>... list) {
//        Object[] objectArray = list;
//        objectArray[1] = Arrays.asList(42); // Pollutes list, no warning
//        for (List<String> ls : list) {
//            for (String string : ls) { // ClassCastException on 42
//                System.out.println(string);
//            }
//        }
//    }
//    
//    public static void main(String[] args) {
//        List<String> s = Arrays.asList("foo", "bar");
//        List<String> s2 = Arrays.asList("baz", "quux");
//        modify( s, s2); // Unchecked varargs warning
//    }
    // Noncompliant code 2
//    public static void modify(List<String>[] list) {
//        Object[] objectArray = list; // Valid
//        objectArray[1] = Arrays.asList(42); // Pollutes list, no warning
//        for (List<String> ls : list) {
//            for (String string : ls) { // ClassCastException on 42
//                System.out.println(string);
//            }
//        }
//    }
//    
//    public static void main(String[] args) {
//        List<String> s = Arrays.asList("foo", "bar");
//        List<String> s2 = Arrays.asList("baz", "quux");
//        List list[] = {s, s2};
//        modify(list); // Unchecked conversion warning
//    }

    // Compliant code solution
    public static void modify(List<List<String>> list) {
        list.set(1, Arrays.asList("forty-two")); // No warning
        for (List<String> ls : list) {
            for (String string : ls) { // ClassCastException on 42
                System.out.println(string);
            }
        }
    }

    public static void main(String[] args) {
        List<String> s = Arrays.asList("foo", "bar");
        List<String> s2 = Arrays.asList("baz", "quux");
        List<List<String>> list = new ArrayList<List<String>>();
        list.add(s);
        list.add(s2);
        modify(list);
    }
}

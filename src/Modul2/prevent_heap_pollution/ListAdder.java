/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Modul2.prevent_heap_pollution;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author REGI
 */
public class ListAdder {
    // Noncompliant code
//    @SuppressWarnings("unchecked")
//    private static void addToList(List list, Object obj) {
//        list.add(obj); // Unchecked warning suppressed
//    }
//    
//    private static <T> void printNum(T type) {
//        if (!(type instanceof Integer || type instanceof Double)) {
//            System.out.println("Cannot print in the supplied type");
//        }
//        List<T> list = new ArrayList<T>();
//        addToList(list, 42);
//        System.out.println(list.get(0));
//    }
//    
//    public static void main(String[] args) {
//        double d = 42;
//        int i = 42;
//        System.out.println(d);
//        ListAdder.printNum(d);
//        System.out.println(i);
//        ListAdder.printNum(i);
//    }

    // Compliant code solution
    private static <T> void addToList(List<T> list, T t) {
        list.add(t); // No warning generated
    }

    private static <T> void printNum(T type) {
        if (type instanceof Integer) {
            List<Integer> list = new ArrayList<Integer>();
            addToList(list, 42);
            System.out.println(list.get(0));
        } else if (type instanceof Double) {
            List<Double> list = new ArrayList<Double>();
            addToList(list, 42.0); // Will not compile with 42 instead of 42.0
            System.out.println(list.get(0));
        } else {
            System.out.println("Cannot print in the supplied type");
        }
    }

    public static void main(String[] args) {
        double d = 42;
        int i = 42;
        System.out.println(d);
        ListAdder.printNum(d);
        System.out.println(i);
        ListAdder.printNum(i);
    }
}

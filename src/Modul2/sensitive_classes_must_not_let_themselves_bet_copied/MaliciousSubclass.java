/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Modul2.sensitive_classes_must_not_let_themselves_bet_copied;

/**
 *
 * @author REGI
 */
public class MaliciousSubclass extends SensitiveClass implements Cloneable {

    public MaliciousSubclass(String filename) {
        super(filename);
    }

    @Override
    public MaliciousSubclass clone() { // Well-behaved clone() method
        MaliciousSubclass s = null;
        try {
            s = (MaliciousSubclass) super.clone();
        } catch (Exception e) {
            System.out.println("not cloneable");
        }
        return s;
    }

    public static void main(String[] args) {
        MaliciousSubclass ms1 = new MaliciousSubclass("file.txt");
        MaliciousSubclass ms2 = ms1.clone(); // Creates a copy
        String s = ms1.get(); // Returns filename
        System.out.println(s); // Filename is "file.txt"
        ms2.replace(); // Replaces all characters with 'x'
        // Both ms1.get() and ms2.get() will subsequently return filename ='xxxxxxxx'
        ms1.printFilename(); // Filename becomes 'xxxxxxxx'
        ms2.printFilename(); // Filename becomes 'xxxxxxxx'
    }
}

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Modul2.sensitive_classes_must_not_let_themselves_bet_copied;

/**
 *
 * @author REGI
 */
public class SensitiveClass {
    // ...

    public final SensitiveClass clone()
            throws CloneNotSupportedException {
        throw new CloneNotSupportedException();
    }
    // Noncompliant code 1
    private char[] filename;
    private Boolean shared = false;

    SensitiveClass(String filename) {
        this.filename = filename.toCharArray();
    }

    final void replace() {
        if (!shared) {
            for (int i = 0; i < filename.length; i++) {
                filename[i] = 'x';
            }
        }
    }

    final String get() {
        if (!shared) {
            shared = true;
            return String.valueOf(filename);
        } else {
            throw new IllegalStateException("Failed to get instance");
        }
    }

    final void printFilename() {
        System.out.println(String.valueOf(filename));
    }
}

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Modul2.dont_form_string_containing_partial_characters_from_variable_width_encoding;

import java.io.IOException;
import java.io.InputStream;
import java.net.Socket;

/**
 *
 * @author REGI
 */
public class Read {
    // Noncompliant code
//    public final int MAX_SIZE = 1024;
//    public String readBytes(Socket socket) throws IOException {
//        InputStream in = socket.getInputStream();
//        byte[] data = new byte[MAX_SIZE+1];
//        int offset = 0;
//        int bytesRead = 0;
//        String str = new String();
//        while ((bytesRead = in.read(data, offset, data.length - offset)) != -1){
//            str += new String(data, offset, bytesRead, "UTF-8");
//            offset += bytesRead;
//            if (offset >= data.length) {
//                throw new IOException("Too much input");
//            }
//        }
//        in.close();
//        return str;
//    }

    // Compliant code
    public final int MAX_SIZE = 1024;

    public String readBytes(Socket socket) throws IOException {
        InputStream in = socket.getInputStream();
        byte[] data = new byte[MAX_SIZE + 1];
        int offset = 0;
        int bytesRead = 0;
        while ((bytesRead = in.read(data, offset, data.length - offset)) != -1) {
            offset += bytesRead;
            if (offset >= data.length) {
                throw new IOException("Too much input");
            }
        }
        String str = new String(data, 0, offset, "UTF-8");
        in.close();
        return str;
    }
}

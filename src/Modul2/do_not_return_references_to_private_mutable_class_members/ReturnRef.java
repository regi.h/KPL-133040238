/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Modul2.do_not_return_references_to_private_mutable_class_members;

import java.util.Hashtable;

/**
 *
 * @author REGI
 */
public class ReturnRef {
    // Bagian 3
    // Noncompliant code
    // Internal state, may contain sensitive data

    private Hashtable<Integer, String> ht = new Hashtable<Integer, String>();

    private ReturnRef() {
        ht.put(1, "123-45-6666");
    }

//    public Hashtable<Integer,String> getValues(){
//        return ht;
//    }
//    
//    public static void main(String[] args) {
//        ReturnRef rr = new ReturnRef();
//        Hashtable<Integer, String> ht1 = rr.getValues(); // Prints sensitivedata 123-45-6666
//        ht1.remove(1); // Untrusted caller can remove entries
//        Hashtable<Integer, String> ht2 = rr.getValues(); // Now prints null; original entry is removed
//    }
    // Compliant code
    // ...
    private Hashtable<Integer, String> getValues() {
        return (Hashtable<Integer, String>) ht.clone(); // Shallow copy
    }

    public static void main(String[] args) {
        ReturnRef rr = new ReturnRef();
        // Prints nonsensitive data
        Hashtable<Integer, String> ht1 = rr.getValues();
        // Untrusted caller can only modify copy
        ht1.remove(1);
        // Prints nonsensitive data
        Hashtable<Integer, String> ht2 = rr.getValues();
    }
}

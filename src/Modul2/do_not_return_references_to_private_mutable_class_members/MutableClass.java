/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Modul2.do_not_return_references_to_private_mutable_class_members;

import java.util.Date;

/**
 *
 * @author REGI
 */
public class MutableClass {
    // Bagian 1
    // Noncompliant code
//    private Date d;
//    public MutableClass() {
//        d = new Date();
//    }

//    public Date getDate() {
//        return d;
//    }
    //Compliant code
//    public Date getDate() {
//        return (Date)d.clone();
//    }
    // Bagian 2
    // Noncompliant code
//    private Date[] date;
//    public MutableClass() {
//        date = new Date[20];
//        for (int i = 0; i < date.length; i++) {
//            date[i] = new Date();
//        }
//    }
//    
//    public Date[] getDate() {
//        return date; // Or return date.clone()
//    }
    // Compliant code solution Depp copy
    private Date[] date;

    public MutableClass() {
        date = new Date[20];
        for (int i = 0; i < date.length; i++) {
            date[i] = new Date();
        }
    }

    public Date[] getDate() {
        Date[] dates = new Date[date.length];
        for (int i = 0; i < date.length; i++) {
            dates[i] = (Date) date[i].clone();
        }
        return dates;
    }
}

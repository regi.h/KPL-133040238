/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Modul2.defensively_copy_mutable_inputs_and_mutable_internal_components;

import java.util.ArrayList;
import java.util.Collection;

/**
 *
 * @author REGI
 */
public class MutableDemo {
    // Bagian 1
    // Noncompliant code
    // java.net.HttpCookie is mutable
//    public void useMutableInput(HttpCookie cookie) {
//        if (cookie == null) {
//            throw new NullPointerException();
//        }
//        // Check whether cookie has expired
//        if (cookie.hasExpired()) {
//        // Cookie is no longer valid; handle condition by throwing an exception
//        }
//     // Cookie may have expired since time of check
//        doLogic(cookie);
//    }

    // Compliant code 1
    // java.net.HttpCookie is mutable 1
//    public void useMutableInput(HttpCookie cookie) {
//        if (cookie == null) {
//            throw new NullPointerException();
//        }
//    
//    // Create copy
//    cookie = (HttpCookie)cookie.clone();
//    // Check whether cookie has expired
//    if (cookie.hasExpired()) {
//// Cookie is no longer valid; handle condition by throwing an exception
//    }
//    doLogic(cookie);
//    }
//
//    private void doLogic(HttpCookie cookie) {
//        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
//    }
    // Compliant code 2
//    public void deepCopy(int[] ints, HttpCookie[] cookies) {
//        if (ints == null || cookies == null) {
//            throw new NullPointerException();
//        }
//        // Shallow copy
//        int[] intsCopy = ints.clone();
//        // Deep copy
//        HttpCookie[] cookiesCopy = new HttpCookie[cookies.length];
//        for (int i = 0; i < cookies.length; i++) {
//        // Manually create copy of each element in array
//        cookiesCopy[i] = (HttpCookie)cookies[i].clone();
//        }
//        doLogic(intsCopy, cookiesCopy);
//    }
//
//    private void doLogic(int[] intsCopy, HttpCookie[] cookiesCopy) {
//        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
//    }
    // Noncompliant code 2
//    public void copyInterfaceInput(Collection<String> collection) {
//        doLogic(collection.clone());
//    }
    //compliant code
    public void copyInterfaceInput(Collection<String> collection) {
        // Convert input to trusted implementation
        collection = new ArrayList(collection);
        doLogic(collection);
    }

    private void doLogic(Collection<String> collection) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Modul2.compare_classes_and_not_class_names;

/**
 *
 * @author REGI
 */
public class auth {

    public static void main(String[] args) {
        // Noncompliant code 1
//        auth auth = new auth();
//        // Determine whether object auth has required/expected class object
//        if (auth.getClass().getName().equals("Modul2.compare_classes_and_not_class_names.auth.DefaultAuthenticationHandler")) {
//            System.out.println("TRUE");
//        }else{
//            System.out.println("FALSE");
//        }

        // Compliant code 1
        auth auth = new auth();
        if (auth.getClass() == Modul2.compare_classes_and_not_class_names.auth.class) {
            System.out.println("TRUE");
        } else {
            System.out.println("FALSE");
        }

        // Noncompliant code 2
        auth x = new auth();
        auth y = new auth();
        // Determine whether objects x and y have the same class name
        if (x.getClass().getName().equals(y.getClass().getName())) {
            // Objects have the same class
            System.out.println("Same Class");
        } else {
            System.out.println("Not same Class");
        }

        // Compliant code 2
        if (x.getClass() == y.getClass()) {
            // Objects have the same class
            System.out.println("Same Class");
        } else {
            System.out.println("Not same Class");
        }
    }
}

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Modul2.do_not_expose_private_members_of_an_outer_class_from_within_a_nested_class;

/**
 *
 * @author REGI
 */
public class Coordinates {

    private int x;
    private int y;
    // Noncompliant code
//    public class Point {
//        public void getPoint() {
//            System.out.println("(" + x + "," + y + ")");
//        }
//    }

    // Compliant code
    private class Point {

        private void getPoint() {
            System.out.println("(" + x + "," + y + ")");
        }
    }
}

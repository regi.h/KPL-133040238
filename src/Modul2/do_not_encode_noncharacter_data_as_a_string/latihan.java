/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Modul2.do_not_encode_noncharacter_data_as_a_string;

import java.math.BigInteger;

/**
 *
 * @author REGI
 */
public class latihan {

    public static void main(String[] args) {
        // Noncompliant code
//        BigInteger x = new BigInteger("530500452766");
//        byte[] byteArray = x.toByteArray();
//        String s = new String(byteArray);
//        byteArray = s.getBytes();
//        x = new BigInteger(byteArray); // hasil 149830058370101340468658109

        // Compliant code solution 1
//        BigInteger x = new BigInteger("530500452766");
//        String s = x.toString(); // Valid character data
//        byte[] byteArray = s.getBytes();
//        String ns = new String(byteArray);
//        x = new BigInteger(ns); // hasil 530500452766

        // Compliant code solution 2
        BigInteger x = new BigInteger("530500452766");
        byte[] byteArray = x.toByteArray();
        String s = Base64.getEncoder().encodeToString(byteArray);
        byteArray = Base64.getDecoder().decode(s);
        x = new BigInteger(byteArray);
        System.out.println(x); // hasil 530500452766
    }
}

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Modul2.ensure_that_references_to_mutable_objects_are_not_exposed;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

/**
 *
 * @author REGI
 */
public class latihan {
    // Noncompliant code //
    //exposing a public static final object allows clients to modify the
    //contents of the object (although they will not be able to change
    //the object itself, as it is final)
//    public static final SomeType [] SOMETHINGS = { ... };

    // Compliant code 1
//    private static final SomeType [] SOMETHINGS = { ... };
//    public static final SomeType [] somethings() {
//        return SOMETHINGS.clone();
//    }
    // Compliant code 2
    private static final SomeType[] THE_THINGS = {...};
    public static final List<SomeType> SOMETHINGS = Collections.unmodifiableList(Arrays.asList(THE_THINGS));
}
